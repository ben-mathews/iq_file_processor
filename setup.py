import setuptools
from glob import glob

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="iq_file_processor",
    version="0.0.1",
    author="Ben Mathews",
    author_email="ben@nothing.com",
    description="A Python-based tool for importing, inspecting, filtering, and exporting IQ data",
    install_requires=[
        "scipy>=1.2.1",
        "numpy>=1.16.3",
        "PyQt5>=5.12.1",
        "pyqtgraph>=0.13.7"
    ],
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ben-mathews/iq_file_processor",
    package_dir={'': 'src'},
    packages=setuptools.find_packages('src'),
    package_data = {
        '': ['*.ui'],
        'dialogs': ['*.ui']
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': [
            'iq_file_processor = iq_file_processor.__main__:main'
        ]
    },
)
