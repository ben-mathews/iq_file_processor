# IQ File Processor

IQ File Processor is a Python-based graphical tool that allows users to import, view, manipulate, and export IQ files of various formats.

![Screenshot](doc/images/screenshot.png)

## Why IQ File Processor?
DSP engineers frequently need to view and manipulate files containing IQ data.  Tools for performing these kinds of operations tend to be command-line based, require manual entry of parameters, and are frequently less than intuitive.  For example, to extract a signal at a specific time and frequency from a file, an engineer must typically import that file into their framework of choice (MATLAB, Python, X-Midas, GNURadio, etc.), transform and plot the data, design an appropriate filter, apply the filter, and export the resulting data back to a file.  These steps can be time-consuming and error-prone.  The goal of IQ File Processor is to make this type of processing quick, easy, and intuitive, allowing DSP engineers to perform operations that used to take minutes in mere seconds.

## Installation
IQ File Processor can be installed using pip.  It requires [Python 3](https://www.python.org/), [PyQt5](https://www.riverbankcomputing.com/software/pyqt/download5), [NumPy](https://www.numpy.org/), [SciPy](https://www.scipy.org/), [Matplotlib](https://matplotlib.org/), and recent version of [PyQtGraph](http://www.pyqtgraph.org/).  To get the required version of PyQtGraph from github, pip must be run with the `--process-dependency-links` switch.

To download and install IQ File Processor, run:

    git clone https://gitlab.com/ben-mathews/iq_file_processor.git
    cd iq_file_processor
    pip install --process-dependency-links .

If installing on Ubuntu 18.04, you may run into issues installing PyQt5.  To work around these, do:

    pip3 install pyqt5 --only-binary pyqt5

## Usage
Launch IQ File Processor from the command line by simply calling

    iq_file_processor
This will bring up the import dialog.  Select the file to import, and optionally update the file metadata in the dialog.  Select OK to import the file.

![Import Dialog](doc/images/usage_01.png)

The main IQ File Utility window will display with various plots of the IQ data.
To select IQ data to view or export, first select one of the four options from the drop-down to specify the filtering scheme (Time, Frequency, Time+Frequency, or None).  
The Select button will enable the graphical controls on the plot that allow users to select time and frequency of the IQ signal.  Once portions of the signal have been selected, the Update button allows the UI can be updated based on the selection, and the Export button allows the IQ for the specified Time and/or Frequency range to be exported.

## Design
IQ File Processor is written in Python, uses PyQt5 as its UI framework, and PyQtGraph as its main plotting framework.  NumPy and SciPy libraries provide the various math and signal processing routines required by the application.  X-MIDAS BLUE file functionality comes from the [RedhawkSDR project](https://github.com/RedhawkSDR).

## Supported File Formats
IQ File Processor currently supports BLUE file formats and raw binary formats.


