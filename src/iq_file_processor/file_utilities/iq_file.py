import os
import fractions
import json
import numpy as np
from scipy import signal
from dsp_utilities.file_handlers import bluefile

from enum import Enum

class IQFileType(Enum):
    RAW = 1
    BLUEFILE = 2
    NPYFILE = 3
    NPZFILE = 4


class IQDataFormat(Enum):
    INT8 = 1
    INT16 = 2
    FLOAT32 = 3
    DOUBLE64 = 4


class IQDataOrder(Enum):
    IQ = 1
    QI = 2


class IQFile:
    def __init__(self, filename, file_type=None, iq_data_format=None, iq_data_order=None, iq=None, fs_hz=None, t_start=None, center_frequency_hz=None, num_samples=None, header_ignore_bytes=None):
        self.filename = filename
        self.file_type = file_type
        self.iq_data_format = iq_data_format
        self.iq_data_order = iq_data_order
        self.iq = iq
        self.fs_hz = fs_hz
        self.t_start_sec = t_start
        self.center_frequency_hz = center_frequency_hz
        self.num_samples = num_samples
        self.header_ignore_bytes = header_ignore_bytes
        self.t = None


    def sample_size_bytes(self):
        if self.iq_data_format == IQDataFormat.INT8:
            return 2
        elif self.iq_data_format == IQDataFormat.INT16:
            return 4
        elif self.iq_data_format == IQDataFormat.FLOAT32:
            return 8
        elif self.iq_data_format == IQDataFormat.DOUBLE64:
            return 16
        else:
            raise Exception('Unhandled file format: ' + str(self.self.file_format))


    def read_metadata(self):
        if self.file_type == IQFileType.RAW:
            if self.header_ignore_bytes == None:
                self.header_ignore_bytes = 0
            if self.iq_data_format == None:
                self.iq_data_format = IQDataFormat.INT16
            if self.iq_data_order == None:
                self.iq_data_order = IQDataOrder.IQ
            file_size_bytes = os.path.getsize(self.filename)
            self.num_samples = int((file_size_bytes - self.header_ignore_bytes) / self.sample_size_bytes())
            self.iq_data_order = IQDataOrder.IQ
            self.fs_hz = 1.0
            self.t_start_sec = 0.0
            self.center_frequency_hz = 0.0

        elif self.file_type == IQFileType.BLUEFILE:
            hdr = bluefile.readheader(self.filename)
            self.header_ignore_bytes = 0
            self.num_samples = hdr['size']
            self.fs_hz = 1.0 / hdr['xdelta']
            self.t_start_sec = hdr['xstart']
            self.center_frequency_hz = 0.0
            self.iq_data_order = IQDataOrder.IQ
            if hdr['format'] == 'CB':
                self.iq_data_format = IQDataFormat.INT8
            elif hdr['format'] == 'CI':
                self.iq_data_format = IQDataFormat.INT16
            elif hdr['format'] == 'CF':
                self.iq_data_format = IQDataFormat.FLOAT32
            elif hdr['format'] == 'CD':
                self.iq_data_format = IQDataFormat.DOUBLE64
            else:
                raise Exception('Unhandled BlueFile format: ' + hdr['format'])

        elif self.file_type == IQFileType.NPYFILE:
            ttt = 1

        elif self.file_type == IQFileType.NPZFILE:
            ttt = 1
        else:
            raise Exception('Unhandled file type: ' + str(self.file_type))


    def read_file(self):
        if self.file_type == IQFileType.RAW:

            if self.iq_data_format == IQDataFormat.INT16:
                # Import from file
                real_samples = np.fromfile(self.filename, dtype=np.int16, count=int(2 * self.num_samples + self.header_ignore_bytes / np.dtype(np.int16).itemsize))

                # Remove header bytes
                real_samples = real_samples[int(self.header_ignore_bytes / np.dtype(np.int16).itemsize):]
            else:
                raise Exception('Unhandled file format type: ' + str(self.iq_data_format))

            # Convert to complex and return
            if self.iq_data_order == IQDataOrder.IQ:
                self.iq = real_samples[0::2] + 1j * real_samples[1::2]
            else:
                self.iq = real_samples[1::2] + 1j * real_samples[0::2]

        elif self.file_type == IQFileType.BLUEFILE:
            hdr, iq_array = bluefile.read(self.filename, end=self.num_samples, ext_header_type=str)
            if 'ext_header' in hdr and len(hdr['ext_header'])>0:
                hdr['ext_header'] = json.loads(hdr['ext_header'])
            if 'center_frequency_hz' in hdr['ext_header']:
                self.center_frequency_hz = hdr['ext_header']['center_frequency_hz']
            self.iq = np.double(iq_array[:,0]) + 1j* np.double(iq_array[:,1])

        elif self.file_type == IQFileType.NPYFILE:
            raise Exception('Not yet implemented')

        elif self.file_type == IQFileType.NPZFILE:
            raise Exception('Not yet implemented')

        else:
            raise Exception('Unhandled file type: ' + str(self.file_type))

        self.generate_t_vector()


    def write_file(self):
        if self.file_type == IQFileType.RAW:
            if self.iq_data_format == IQDataFormat.INT16:
                real_samples = np.empty((2 * len(self.iq),), dtype=np.int16)
                if self.iq_data_order == IQDataOrder.IQ:
                    real_samples[0::2] = np.real(self.iq).astype(np.int16)
                    real_samples[1::2] = np.imag(self.iq).astype(np.int16)
                else:
                    real_samples[0::2] = np.imag(self.iq).astype(np.int16)
                    real_samples[1::2] = np.real(self.iq).astype(np.int16)
            else:
                raise Exception('Unhandled file format type: ' + str(self.iq_data_format))
            if os.path.isfile(self.filename):
                os.remove(self.filename)
            real_samples.tofile(self.filename)

        elif self.file_type == IQFileType.BLUEFILE:
            if self.iq_data_format == IQDataFormat.INT16:
                format='CI'
                data_size = 2 * self.num_samples * np.dtype(np.int16).itemsize
            else:
                raise Exception('Unhandled file format type: ' + str(self.iq_data_format))
            if os.path.isfile(self.filename):
                os.remove(self.filename)
            hdr = bluefile.header(type=1000, format=format, detached=0, xstart=self.t_start_sec, size=self.num_samples, data_size=data_size, data_start=0.0, xdelta=1.0/self.fs_hz)
            hdr['ext_header'] = json.dumps({'center_frequency_hz': self.center_frequency_hz})
            bluefile.write(filename=self.filename, hdr=hdr, ext_header_type=type(hdr['ext_header']), data=np.column_stack((np.real(self.iq), np.imag(self.iq))))

        elif self.file_type == IQFileType.NPYFILE:
            raise Exception('Not yet implemented')

        elif self.file_type == IQFileType.NPZFILE:
            raise Exception('Not yet implemented')

        else:
            raise Exception('Unhandled file type: ' + str(self.file_type))


    def update_filename(self, filename):
        self.filename = filename


    def update_file_type(self, file_type):
        self.file_type = file_type
        """
        file_size_bytes = os.path.getsize(self.filename)
        num_samples = int((file_size_bytes - self.header_ignore_bytes) / self.sample_size_bytes())
        if num_samples < self.num_samples:
            self.num_samples = num_samples
        if self.iq is not None:
            self.read_file()
        if self.t is not None:
            self.generate_t_vector()
        """


    def update_iq_data_format(self, iq_data_format):
        self.iq_data_format = iq_data_format
        #if self.iq is not None and os.path.isfile(self.filename):
        #    self.read_file()
        if self.t is not None:
            self.generate_t_vector()


    def update_iq_data_order(self, iq_data_order):
        self.iq_data_order = iq_data_order
        #if self.iq is not None and os.path.isfile(self.filename):
        #    self.read_file()
        if self.t is not None:
            self.generate_t_vector()


    def update_sample_rate(self, fs_hz):
        self.fs_hz = fs_hz
        if self.t is not None:
            self.generate_t_vector()


    def update_start_time_sec(self, t_start_sec):
        self.t_start_sec = t_start_sec
        self.generate_t_vector()


    def update_center_frequency_hz(self, center_frequency_hz):
        self.center_frequency_hz = center_frequency_hz


    def update_num_samples(self, num_samples):
        self.num_samples = num_samples
        #if self.iq is not None and os.path.isfile(self.filename):
        #    self.read_file()


    def update_header_ignore_bytes(self, header_ignore_bytes):
        self.header_ignore_bytes = header_ignore_bytes
        #if self.iq is not None and os.path.isfile(self.filename):
        #    self.read_file()


    def generate_t_vector(self):
        self.t = self.t_start_sec + np.linspace(0, len(self.iq) - 1, len(self.iq)) / self.fs_hz


    def tune(self, fc_hz):
        if self.t is None:
            self.generate_t_vector()

        assert len(self.t)==len(self.iq), 'IQ and t vectors are not the same length'

        complex_exponential = np.exp(-1j * 2 * np.pi * fc_hz * self.t)
        iq_tuned = self.iq * complex_exponential
        return iq_tuned


    def low_pass_filter(self, cutoff_frequency_hz, numtaps=271, window='hamming'):
        b = signal.firwin(numtaps=numtaps, cutoff=cutoff_frequency_hz/self.fs_hz, window=window)
        iq_filtered = signal.filtfilt(b, [1], self.iq)
        return iq_filtered


    def resample(self, new_fs_hz, denominator_limit=1000000):
        resample_frequency_frac = fractions.Fraction(new_fs_hz / self.fs_hz).limit_denominator(denominator_limit)
        iq_resampled = signal.resample_poly(self.iq, resample_frequency_frac.numerator, resample_frequency_frac.denominator)
        return iq_resampled, resample_frequency_frac


    def time_filter(self, start_time_sec, end_time_sec):
        iq_filtered = self.iq[(self.t >= start_time_sec) & (self.t <= end_time_sec)]
        t_filtered = self.t[(self.t >= start_time_sec) & (self.t <= end_time_sec)]
        return t_filtered, iq_filtered






