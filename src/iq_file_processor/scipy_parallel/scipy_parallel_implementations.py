import numpy as np
from scipy.signal import spectrogram, welch
from scipy.signal._spectral_py import _triage_segments
import threading


def spectrogram_parallel(x, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=-1, mode='psd', num_threads=4):
    window_, nperseg = _triage_segments(window, nperseg, input_length=x.shape[axis])
    if noverlap is None:
        noverlap = nperseg // 8

    if nfft is None:
        nfft = nperseg
    elif nfft < nperseg:
        raise ValueError('nfft must be greater than or equal to nperseg.')
    else:
        nfft = int(nfft)

    nstep = nperseg - noverlap
    sxx_shape = (nperseg, (len(x)-noverlap)//nstep)
    sxx_combined = np.empty(sxx_shape)
    num_spectrogram_blocks = num_threads
    num_segments_per_block = sxx_shape[1] // num_spectrogram_blocks
    num_samples_per_block = nstep*num_segments_per_block + noverlap

    f = []
    t = []
    segment_start_index = []
    segment_end_index = []
    sample_start_index = []
    sample_end_index = []
    for block in range(0, num_spectrogram_blocks):
        if block == num_spectrogram_blocks - 1:
            sample_start_index.append(block * nstep * num_segments_per_block)
            sample_end_index.append(len(x))
        else:
            sample_start_index.append(block * nstep * num_segments_per_block)
            sample_end_index.append(block * nstep * num_segments_per_block + num_samples_per_block)
        segment_start_index.append(block * num_segments_per_block)
        segment_end_index.append(int((sample_end_index[-1]+1-noverlap)/nstep))
        t.append(np.asarray([]))
        f.append(np.asarray([]))

    spectrogram_threads = []#
    for thread_index in range(0, num_threads):
        spectrogram_threads.append(
            threading.Thread(target=_spectrogram_parallel_helper_function,
                             args=(f, t, sxx_combined, thread_index, segment_start_index[thread_index],
                                   segment_end_index[thread_index], x[sample_start_index[thread_index]:sample_end_index[thread_index]], fs, window, nperseg,
                                   noverlap, nfft, detrend, return_onesided, scaling, axis, mode)))

    for welch_thread in spectrogram_threads:
        welch_thread.start()
    for welch_thread in spectrogram_threads:
        welch_thread.join()

    f_combined = f[0]
    for t_index in range(0, len(t)):
        t[t_index] = t[t_index] + sample_start_index[t_index]/fs
    t_combined = np.concatenate(t)
    sxx_combined

    return f_combined, t_combined, sxx_combined


def _spectrogram_parallel_helper_function(f, t, sxx, index, segment_start_index, segment_end_index, x, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=-1, mode='psd'):
    f[index], t[index], sxx[:, segment_start_index:segment_end_index] = spectrogram(x=x, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap, nfft=nfft, detrend=detrend, return_onesided=return_onesided, scaling=scaling, axis=axis, mode=mode)
    

def welch_parallel(x, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=-1, average='mean', num_threads=4):
    f = []
    Pxx = []
    for block_num in range(0, num_threads):
        f.append(np.array([]))
        Pxx.append(np.array([]))
    welch_block_size = int(len(x) / num_threads)

    welch_threads = []
    for thread_index in range(0, num_threads):
        welch_threads.append(threading.Thread(target=_welch_parallel_helper_function,
                                              args=(f, Pxx, thread_index, x[(thread_index * welch_block_size):((thread_index + 1) * welch_block_size)],
                                                    fs, window, nperseg, noverlap, nfft, detrend, return_onesided, scaling, axis, average)))
    for welch_thread in welch_threads:
        welch_thread.start()
    for welch_thread in welch_threads:
        welch_thread.join()
    Pxx = np.mean(Pxx, 0)
    f = f[0]

    return f, Pxx


def _welch_parallel_helper_function(f, Pxx, index, x, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None, detrend='constant', return_onesided=True, scaling='density', axis=-1, average='mean'):
    f[index], Pxx[index] = welch(x, fs, window, nperseg, noverlap, nfft, detrend, return_onesided, scaling, axis, average)


if __name__ == '__main__':
    iq = np.random.randn(1000000) + 1j* np.random.randn(1000000)
    #iq = np.random.randn(201112064) + 1j * np.random.randn(201112064)

    fs_hz = 1/100
    nfft = 1024

    #f, Pxx = welch(x = iq, fs=fs_hz, window='hann', nperseg=nfft, return_onesided=False, detrend=False)
    #f2, Pxx2 = welch_parallel(x = iq, fs=fs_hz, window='hann', nperseg=nfft, return_onesided=False, detrend=False, num_threads=4)

    f, t, sxx = spectrogram(x=iq, fs=fs_hz, window=('tukey', .25), nperseg=nfft, noverlap=None, nfft=None, detrend='constant', return_onesided=False, scaling='density', axis=-1, mode='psd')
    f2, t2, sxx2 = spectrogram_parallel(x=iq, fs=fs_hz, window=('tukey', .25), nperseg=nfft, noverlap=None, nfft=None, detrend='constant', return_onesided=False, scaling='density', axis=-1, mode='psd', num_threads=4)

    print(str(np.max(f-f2)))
    print(str(np.max(t-t2)))
    print(str(np.max(sxx-sxx2)))


