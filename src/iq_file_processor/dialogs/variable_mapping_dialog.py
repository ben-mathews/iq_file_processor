import os
import copy
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from dsp_utilities.file_handlers.iq_file import IQFile, IQFileType, IQDataFormat, IQDataOrder

from enum import Enum


qtCreatorFile = os.path.dirname(os.path.abspath(__file__)) + '/' + 'variable_mapping_dialog.ui'


class SelectionMode(Enum):
    IMPORT = 1
    EXPORT = 2


class VariableMappingDialog(QDialog):
    def __init__(self, variable_mapping):
        super(VariableMappingDialog, self).__init__()
        loadUi(qtCreatorFile, self)

        if 'iq' in variable_mapping:
            self.txtIQVariableName.setText(variable_mapping['iq'])
        else:
            self.txtIQVariableName.setText('iq')

        if 'fs_hz' in variable_mapping:
            self.txtSampleRateVariableName.setText(variable_mapping['fs_hz'])
        else:
            self.txtSampleRateVariableName.setText('fs_hz')

        if 't_start_sec' in variable_mapping:
            self.txtStartTimeVariableName.setText(variable_mapping['t_start_sec'])
        else:
            self.txtStartTimeVariableName.setText('t_start_sec')

        if 'fs_hz' in variable_mapping:
            self.txtCenterFrequencyVariableName.setText(variable_mapping['center_frequency_hz'])
        else:
            self.txtCenterFrequencyVariableName.setText('center_frequency_hz')



    def accept(self):
        super(VariableMappingDialog, self).accept()

        self.variable_mapping = {
            'iq': self.txtIQVariableName.text(),
            'fs_hz': self.txtSampleRateVariableName.text(),
            't_start_sec': self.txtStartTimeVariableName.text(),
            'center_frequency_hz': self.txtCenterFrequencyVariableName.text()
        }

        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 1
        self.Rejected = 0


    def reject(self):
        super(VariableMappingDialog, self).reject()
        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 0
        self.Rejected = 1


if __name__ == "__main__":
    import sys
    from PyQt5.QtWidgets import QApplication
    app = QApplication(sys.argv)

    variable_names = ["iq," "fs_hz"]
    fsd = VariableMappingDialog(variable_mapping=variable_names)
    fsd.exec_()
    if fsd.Accepted:
        print('Do Something')

