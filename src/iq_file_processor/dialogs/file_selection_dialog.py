import os
import copy
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from dsp_utilities.file_handlers.iq_file import IQFile, IQFileType, IQDataFormat, IQDataOrder

from enum import Enum


qtCreatorFile = os.path.dirname(os.path.abspath(__file__)) + '/' + 'file_selection_dialog.ui'


class SelectionMode(Enum):
    IMPORT = 1
    EXPORT = 2


class FileSelectionDialog(QDialog):
    def __init__(self, selection_mode, iq_file_name = None, iq_file = None, center_frequency_hz = None, fs_hz = None,
                 variable_mapping = {}):
        super(FileSelectionDialog, self).__init__()
        loadUi(qtCreatorFile, self)
        self.iq_file = None

        self.results = lambda: 0

        self.cbFileType.clear()
        self.cbFileType.addItem('Raw', IQFileType.RAW)
        self.cbFileType.addItem('BlueFile', IQFileType.BLUEFILE)
        self.cbFileType.addItem('Numpy NPY', IQFileType.NPYFILE)
        self.cbFileType.addItem('Numpy NPZ', IQFileType.NPZFILE)
        self.cbFileType.addItem('Matlab', IQFileType.MATFILE)
        self.cbFileType.addItem('VITA49', IQFileType.VITA49FILE)

        self.cbDataFormat.clear()
        self.cbDataFormat.addItem('Complex Int8', IQDataFormat.INT8)
        self.cbDataFormat.addItem('Complex Int12', IQDataFormat.INT12)
        self.cbDataFormat.addItem('Complex Int16', IQDataFormat.INT16)
        self.cbDataFormat.addItem('Complex Float32', IQDataFormat.FLOAT32)
        self.cbDataFormat.addItem('Complex Double64', IQDataFormat.DOUBLE64)

        self.cbDataOrder.clear()
        self.cbDataOrder.addItem('I,Q', IQDataOrder.IQ)
        self.cbDataOrder.addItem('Q,I', IQDataOrder.QI)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        self.cbOverrideMetadata.clicked.connect(self.override_metadata_callback)
        self.cbFileType.currentIndexChanged.connect(self.update_file_type_callback)
        self.cbDataFormat.currentIndexChanged.connect(self.update_iq_data_format_callback)
        self.cbDataOrder.currentIndexChanged.connect(self.update_iq_data_order_callback)
        self.btnReadFromFile.clicked.connect(self.update_dialog_from_file)

        self.txtNumSamplesSelection.textEdited.connect(self.update_duration_selection_sec)
        self.txtDurationSelectionSec.textEdited.connect(self.update_num_samples_selection_sec)

        self.update_message_text("")
        self.set_selection_controls_enabled_state(False)

        self.variable_mapping = variable_mapping

        if selection_mode.lower() == 'import':
            self.btnSelect.clicked.connect(self.select_file_for_import)
            self.selection_mode = SelectionMode.IMPORT
            self.txtFileName.setText(iq_file_name)
            self.select_file_for_import()
        else:
            self.btnSelect.clicked.connect(self.select_file_for_export)
            self.selection_mode = SelectionMode.EXPORT
            assert iq_file is not None, "iq_file must be provided for Export mode"
            self.iq_file = copy.copy(iq_file) # Making a shallow copy - make sure we don't do anything to touch the iq data
            self.iq_file.iq = None
            self.iq_file.t_iq_sec = None
            if center_frequency_hz is not None:
                self.iq_file.update_center_frequency_hz(center_frequency_hz=center_frequency_hz)
            if fs_hz is not None:
                self.iq_file.update_sample_rate(fs_hz=fs_hz)
            self.update_dialog_from_file(update_metadata=False)
            self.set_metadata_controls_enabled_state(enabled_state=False, ignore_file_format=True)


    def set_ok_button_state(self, ok_button_state):
        ok_button = [button for button in self.buttonBox.buttons() if button.text().lower().find('ok') != -1]
        if len(ok_button) != 1:
            raise Exception('Expecting 1 Ok button')
        ok_button[0].setEnabled(ok_button_state)


    def accept(self):
        super(FileSelectionDialog, self).accept()
        if self.selection_mode == SelectionMode.IMPORT:
            filename = self.txtFileName.text()
            file_type = self.cbFileType.itemData(self.cbFileType.currentIndex())
            iq_data_format = self.cbDataFormat.itemData(self.cbDataFormat.currentIndex())
            iq_data_order = self.cbDataOrder.itemData(self.cbDataOrder.currentIndex())

            try:
                fs_hz = float(self.txtSampleRateHz.text())
            except:
                fs_hz = 1.0
            try:
                t_start = float(self.txtStartTimeSec.text())
            except:
                t_start = 0.0
            try:
                num_samples = int(self.txtNumSamples.text())
            except:
                num_samples = 0
            try:
                center_frequency_hz = float(self.txtCenterFrequencyHz.text())
            except:
                center_frequency_hz = 0.0
            try:
                header_ignore_bytes = int(self.txtHeaderBytes.text())
            except:
                header_ignore_bytes = 0

            self.iq_file = \
                IQFile(filename=filename, file_type=file_type, iq_data_format=iq_data_format,
                       iq_data_order=iq_data_order, fs_hz=fs_hz, t_start_sec=t_start, num_samples=num_samples,
                       center_frequency_hz=center_frequency_hz, header_ignore_bytes=header_ignore_bytes,
                       variable_mapping=self.variable_mapping)

            self.results.iq_file = self.iq_file

            frequency_offset_hz = float(self.txtAdditionalFrequencyOffsetHz.text())

            flip_spectrum = False
            if self.cbFlipSpectrum.isChecked():
                flip_spectrum = True

            self.results.selection_parameters = {
                'start_time_sec': float(self.txtSelectionStartTimeSec.text()),
                'start_sample': int((float(self.txtSelectionStartTimeSec.text()) - t_start) / fs_hz),
                'num_samples': int(self.txtNumSamplesSelection.text()),
                'end_sample': int((float(self.txtSelectionStartTimeSec.text()) - t_start) / fs_hz) +
                              int(self.txtNumSamplesSelection.text()),
                'frequency_offset_hz': frequency_offset_hz,
                'flip_spectrum': flip_spectrum
            }

        else:
            self.results.iq_file = self.iq_file
            #self.iq_file.write_file()

        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 1
        self.Rejected = 0


    def reject(self):
        super(FileSelectionDialog, self).reject()
        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 0
        self.Rejected = 1


    def reset_input_fields(self):
        enabled_state = False
        self.cbOverrideMetadata.setChecked(enabled_state)
        self.cbOverrideMetadata.isChecked()
        self.txtSampleRateHz.setEnabled(enabled_state)
        self.txtStartTimeSec.setEnabled(enabled_state)
        self.txtNumSamples.setEnabled(enabled_state)
        self.txtHeaderBytes.setEnabled(enabled_state)


    def select_file_for_import(self):
        self.reset_input_fields()
        options = QtWidgets.QFileDialog.Options()
        options = options | QtWidgets.QFileDialog.DontUseNativeDialog # Use this to work around gvfs wonkieness on Ubuntu

        filename_txt = self.txtFileName.text()
        if os.path.exists(filename_txt):
            directory = os.path.dirname(filename_txt)
            filename, filter = \
                QtWidgets.QFileDialog.getOpenFileName(self, caption="Select File to Import",
                                                      initialFilter="All Files (*);;Python Files (*.dat)",
                                                      options=options, directory=directory)
        else:
            filename, filter = \
                QtWidgets.QFileDialog.getOpenFileName(self, caption="Select File to Import",
                                                      initialFilter="All Files (*);;Python Files (*.dat)",
                                                      options=options)

        if filename:
            self.txtFileName.setText(filename)
            file_type = IQFile.guess_file_type(filename)
            if file_type is IQFileType.BLUEFILE:
                self.read_metadata_from_file(filename, file_type=file_type)
            elif file_type is IQFileType.VITA49FILE:
                self.read_metadata_from_file(filename, file_type=file_type)
            elif file_type is IQFileType.MATFILE:
                self.read_metadata_from_file(filename, file_type=file_type)
            elif file_type is IQFileType.RAW:
                self.update_message_text('Data Format, Data Order, and Header Bytes must be manually set for RAW files')
                self.set_metadata_controls_enabled_state(enabled_state=True, ignore_file_format=True)
            else:
                raise Exception('Unhandled file type!')
            self.set_ok_button_state(True)
        else:
            self.set_ok_button_state(False)


    def read_metadata_from_file(self, filename, file_type):
        self.set_selection_controls_enabled_state(False)
        filename_no_extension, file_extension = os.path.splitext(filename)

        if file_type is IQFileType.NPYFILE:
            combobox_index = self.cbFileType.findText('Numpy NPY')
            file_type = IQFileType.NPYFILE
        elif file_type is IQFileType.NPZFILE:
            combobox_index = self.cbFileType.findText('Numpy NPZ')
        elif file_type is IQFileType.BLUEFILE:
            combobox_index = self.cbFileType.findText('BlueFile')
        elif file_type is IQFileType.RAW:
            combobox_index = self.cbFileType.findText('Raw')
        elif file_type is IQFileType.VITA49FILE:
            combobox_index = self.cbFileType.findText('VITA49')
        elif file_type is IQFileType.MATFILE:
            combobox_index = self.cbFileType.findText('Matlab')
        else:
            combobox_index = self.cbFileType.currentIndex()

        # Handle Bluefiles
        self.bluefile_datafilename = None
        if file_type is IQFileType.BLUEFILE:
            if file_extension.lower() == '.tmp' or file_extension.lower() == '.prm':
                if os.path.isfile(filename_no_extension + '.det'):
                    self.bluefile_datafilename = filename_no_extension + '.det'
            if file_extension.lower() == '.det':
                if os.path.isfile(filename_no_extension + '.tmp'):
                    self.bluefile_datafilename = filename
                    filename = filename_no_extension + '.tmp'
                elif os.path.isfile(filename_no_extension + '.prm'):
                    self.bluefile_datafilename = filename
                    filename = filename_no_extension + '.prm'
                else:
                    raise Exception('Detached BLUEFILE file selected but no header found')

        self.iq_file = IQFile(filename=filename, file_type=file_type, variable_mapping=self.variable_mapping)
        self.cbFileType.setCurrentIndex(combobox_index)
        self.update_dialog_from_file()
        self.update_message_text("Enter selection parameters and select Ok")
        self.set_selection_controls_enabled_state(True)


    def update_dialog_from_file(self, update_metadata=True):
        if update_metadata:
            self.iq_file.read_metadata()

        combobox_index = [i for i in range(0, self.cbFileType.count()) if self.cbFileType.itemData(i) == self.iq_file.file_type]
        if len(combobox_index) == 1:
            combobox_index = combobox_index[0]
        else:
            raise Exception('Unhandled file type: ' + str(self.iq_file.file_type))
        self.cbFileType.setCurrentIndex(combobox_index)

        combobox_index = [i for i in range(0, self.cbDataFormat.count()) if self.cbDataFormat.itemData(i) == self.iq_file.iq_data_format]
        if len(combobox_index) == 1:
            combobox_index = combobox_index[0]
        else:
            raise Exception('Unhandled file format: ' + str(self.iq_file.file_type))
        self.cbDataFormat.setCurrentIndex(combobox_index)

        combobox_index = [i for i in range(0, self.cbDataOrder.count()) if self.cbDataOrder.itemData(i) == self.iq_file.iq_data_order]
        if len(combobox_index) == 1:
            combobox_index = combobox_index[0]
        else:
            raise Exception('Unhandled data order: ' + str(self.iq_file.iq_data_order))
        self.cbDataOrder.setCurrentIndex(combobox_index)

        self.txtSampleRateHz.setText(str(self.iq_file.fs_hz))
        self.txtSampleRateHz.home(True)
        self.txtStartTimeSec.setText(str(self.iq_file.t_start_sec))
        self.txtStartTimeSec.home(True)
        self.txtStartTimeDTG.setText(self.iq_file.timecode_datetime_str)
        self.txtStartTimeDTG.home(True)
        self.txtDurationSec.setText(str(self.iq_file.num_samples / self.iq_file.fs_hz))
        self.txtDurationSec.home(True)
        self.txtCenterFrequencyHz.setText(str(self.iq_file.center_frequency_hz))
        self.txtCenterFrequencyHz.home(True)
        self.txtNumSamples.setText(str(self.iq_file.num_samples))
        self.txtNumSamples.home(True)
        self.txtHeaderBytes.setText(str(self.iq_file.header_ignore_bytes))
        self.txtHeaderBytes.home(True)

        selection_time_sec = 1.0
        num_samples_selection = min([int(selection_time_sec * self.iq_file.fs_hz), int(self.txtNumSamples.text())])
        self.txtSelectionStartTimeSec.setText(str(self.iq_file.t_start_sec))
        self.txtSelectionStartTimeSec.home(True)
        self.txtNumSamplesSelection.setText(str(num_samples_selection))
        self.txtSelectionStartTimeSec.home(True)
        self.update_duration_selection_sec(str(num_samples_selection))


    def update_file_from_dialog(self):
        self.iq_file.update_filename(file_name=self.txtFileName.getText())
        self.iq_file.update_file_type(file_type=self.cbFileType.itemData(self.cbFileType.currentIndex()))
        self.iq_file.update_iq_data_format(iq_data_format=self.cbDataFormat.itemData(self.cbDataFormat.currentIndex()))
        self.iq_file.update_iq_data_order(iq_data_order=self.cbDataOrder.itemData(self.cbDataOrder.currentIndex()))
        self.iq_file.update_sample_rate(fs_hz=float(self.txtSampleRateHz.text()))
        self.iq_file.update_start_time(t_start_sec=float(self.txtStartTimeSec.text()))
        self.iq_file.update_center_frequency_hz(center_frequency_hz=float(self.txtCenterFrequencyHz.text()))
        self.iq_file.update_num_samples(num_samples=int(self.txtNumSamples.text()))
        self.iq_file.update_header_ignore_bytes(header_ignore_bytes=int(self.txtHeaderBytes.text()))


    def override_metadata_callback(self):
        enabled_state = False
        if self.cbOverrideMetadata.isChecked():
            enabled_state = True
        self.set_metadata_controls_enabled_state(enabled_state)


    def set_metadata_controls_enabled_state(self, enabled_state, ignore_file_format=False):
        self.cbFileType.setEnabled(enabled_state)
        selected_file_format = self.cbFileType.itemData(self.cbFileType.currentIndex())
        if ignore_file_format or (selected_file_format == IQFileType.RAW and enabled_state == True):
            self.cbDataFormat.setEnabled(enabled_state)
            self.cbDataOrder.setEnabled(enabled_state)
        if self.selection_mode == SelectionMode.IMPORT:
            self.txtSampleRateHz.setEnabled(enabled_state)
            self.txtStartTimeSec.setEnabled(enabled_state)
            self.txtCenterFrequencyHz.setEnabled(enabled_state)
            self.txtNumSamples.setEnabled(enabled_state)
            self.txtHeaderBytes.setEnabled(enabled_state)
        else:
            self.txtSampleRateHz.setEnabled(False)
            self.txtStartTimeSec.setEnabled(False)
            self.txtCenterFrequencyHz.setEnabled(False)
            self.txtNumSamples.setEnabled(False)
            self.txtHeaderBytes.setEnabled(False)


    def set_selection_controls_enabled_state(self, enabled_state):
        self.txtSelectionStartTimeSec.setEnabled(enabled_state)
        self.txtNumSamplesSelection.setEnabled(enabled_state)
        self.txtDurationSelectionSec.setEnabled(enabled_state)


    def select_file_for_export(self):
        options = QtWidgets.QFileDialog.Options()
        filename, filter = QtWidgets.QFileDialog.getSaveFileName(self, caption="Save file", filter="BLUE File (*.tmp *.prm);;Raw File (*.raw *.dat);;All files (*.*)", initialFilter="All files (*.*)", options=options)
        if filename:
            self.txtFileName.setText(filename)
            filename_no_extension, file_extension = os.path.splitext(filename)
            file_type = None
            if file_extension.lower() == '.npy':
                file_type = IQFileType.NPYFILE
            elif file_extension.lower() == '.npz' or file_extension.lower() == '.npyz':  # Numpy npz format
                file_type = IQFileType.NPZFILE
            elif file_extension.lower() == '.det' or file_extension.lower() == '.prm' or file_extension.lower() == '.tmp':  # Bluefile format
                file_type = IQFileType.BLUEFILE
            elif file_extension.lower() == '.dat' or file_extension.lower() == '.raw':  # Raw format
                file_type = IQFileType.RAW
            elif file_extension.lower() == '.mat':  # MATLAB format
                file_type = IQFileType.MATFILE
            else:
                file_type = IQFileType.RAW

            self.iq_file.update_filename(filename=filename)
            self.iq_file.update_file_type(file_type)

            self.update_dialog_from_file(update_metadata=False)


    def update_file_type_callback(self):
        file_type = self.cbFileType.itemData(self.cbFileType.currentIndex())
        if self.iq_file is not None:
            self.iq_file.update_file_type(file_type)


    def update_iq_data_format_callback(self):
        iq_data_format = self.cbDataFormat.itemData(self.cbDataFormat.currentIndex())
        if self.iq_file is not None:
            self.iq_file.update_iq_data_format(iq_data_format)


    def update_iq_data_order_callback(self):
        iq_data_order = self.cbDataOrder.itemData(self.cbDataOrder.currentIndex())
        if self.iq_file is not None:
            self.iq_file.update_iq_data_order(iq_data_order)


    def update_sample_rate(self, fs_hz):
        self.fs_hz = fs_hz
        if self.t is not None:
            self.generate_t_vector()


    def update_start_time_sec(self, t_start_sec):
        self.t_start_sec = t_start_sec
        if self.t is not None:
            self.generate_t_vector()


    def update_num_samples(self, num_samples):
        self.num_samples = num_samples
        if self.iq is not None and self.selection_mode == SelectionMode.IMPORT:
            self.read_file()


    def update_duration_selection_sec(self, new_num_samples):
        try:
            self.txtDurationSelectionSec.setText(str(int(new_num_samples) / self.iq_file.fs_hz))
        except:
            self.txtDurationSelectionSec.setText("-")
        self.txtDurationSelectionSec.setCursorPosition(0)
        self.set_selection_font_styles()


    def update_num_samples_selection_sec(self, new_duration_sec):
        try:
            self.txtNumSamplesSelection.setText(str(int(float(new_duration_sec) * self.iq_file.fs_hz)))
        except:
            self.txtNumSamplesSelection.setText("-")
        self.txtNumSamplesSelection.setCursorPosition(0)
        self.set_selection_font_styles()


    def set_selection_font_styles(self):
        if int(int(self.txtNumSamplesSelection.text())) > self.iq_file.num_samples:
            self.txtDurationSelectionSec.setStyleSheet('''
                            QLineEdit {
                                border: 2px solid rgb(63, 63, 63);
                                color: rgb(255, 255, 255);
                                background-color: rgb(255, 0, 0);
                            }
                        ''')
            self.txtNumSamplesSelection.setStyleSheet('''
                            QLineEdit {
                                border: 2px solid rgb(63, 63, 63);
                                color: rgb(255, 255, 255);
                                background-color: rgb(255, 0, 0);
                            }
                        ''')
        else:
            self.txtDurationSelectionSec.setStyleSheet('')
            self.txtNumSamplesSelection.setStyleSheet('')


    def update_message_text(self, text):
        self.statusbar.showMessage(text)


if __name__ == "__main__":
    import sys
    from PyQt5.QtWidgets import QApplication
    app = QApplication(sys.argv)

    fsd = FileSelectionDialog(selection_mode="import")
    fsd.exec_()
    if fsd.Accepted:
        iq_file = fsd.results.iq_file

