import os
from enum import Enum
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.uic import loadUi


qtCreatorFile = os.path.dirname(os.path.abspath(__file__)) + '/' + 'selection_and_processing_parameters_dialog.ui'

class FrequencyUnits(Enum):
    Hz = 0
    KHz = 1
    MHz = 2
    GHz = 3


class SelectionAndProcessingParametersDialog(QDialog):
    def __init__(self, center_frequency_hz, bandwidth_hz, start_time_sec, start_time_dtg, end_time_sec, num_taps):
        super(SelectionAndProcessingParametersDialog, self).__init__()
        loadUi(qtCreatorFile, self)

        for cb_index in range(0, self.cbFrequencyUnits.count()):
            if self.cbFrequencyUnits.itemText(cb_index) == 'Hz':
                self.cbFrequencyUnits.setItemData(cb_index, FrequencyUnits.Hz)
            elif self.cbFrequencyUnits.itemText(cb_index) == 'KHz':
                self.cbFrequencyUnits.setItemData(cb_index, FrequencyUnits.KHz)
            elif self.cbFrequencyUnits.itemText(cb_index) == 'MHz':
                self.cbFrequencyUnits.setItemData(cb_index, FrequencyUnits.MHz)
            elif self.cbFrequencyUnits.itemText(cb_index) == 'GHz':
                self.cbFrequencyUnits.setItemData(cb_index, FrequencyUnits.GHz)
            else:
                raise Exception('Unhandled data format: ' + self.cbFrequencyUnits.itemText(cb_index))

        self.txtStartTimeSec.textEdited.connect(self.update_start_time_dtg)
        self.cbFrequencyUnits.currentIndexChanged.connect(self.frequency_units_changed_callback)

        self.set_frequency_units('MHz')
        self.set_center_frequency(center_frequency_hz=center_frequency_hz)
        self.set_bandwidth(bandwidth_hz=bandwidth_hz)
        self.set_num_taps(num_taps=num_taps)
        self.original_start_time_sec = start_time_sec
        self.set_start_time_sec(start_time_sec=start_time_sec)
        self.original_start_time_dtg = start_time_dtg
        self.set_start_time_dtg(start_time_dtg=start_time_dtg)
        self.set_end_time_sec(end_time_sec=end_time_sec)
        #10 ** (self.settings.frequency_units.value * 3)


    def accept(self):
        super(SelectionAndProcessingParametersDialog, self).accept()
        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 1
        self.Rejected = 0

        self.center_frequency_hz = self.get_center_frequency()
        self.bandwidth_hz = self.get_bandwidth()
        self.start_time_sec = self.get_start_time_sec()
        self.end_time_sec = self.get_end_time_sec()
        self.num_taps = self.get_num_taps()


    def reject(self):
        super(SelectionAndProcessingParametersDialog, self).reject()
        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 0
        self.Rejected = 1


    def update_frequencies(self, new_frequency_units):
        self.set_center_frequency(self.get_center_frequency(), frequency_units=new_frequency_units)
        self.set_bandwidth(self.get_bandwidth(), frequency_units=new_frequency_units)


    def frequency_units_changed_callback(self):
        new_frequency_units = self.cbFrequencyUnits.itemData(self.cbFrequencyUnits.currentIndex())
        self.update_frequencies(new_frequency_units)
        self.frequency_units = self.cbFrequencyUnits.itemData(self.cbFrequencyUnits.currentIndex())


    def get_frequency_units(self):
        frequency_units = self.cbFrequencyUnits.itemData(self.cbFrequencyUnits.currentIndex())
        return frequency_units


    def set_frequency_units(self, frequency_units_str):
        frequency_units = [val for val in FrequencyUnits if val.name == frequency_units_str]
        if len(frequency_units) != 1:
            raise Exception('Unhandled condition in selecting frequency units.  Can not find mapping for unit: ' + frequency_units_str)
        self.frequency_units = frequency_units[0]

        cb_index = self.cbFrequencyUnits.findText(self.frequency_units.name)
        if cb_index == -1:
            raise Exception('Unhandled condition in selecting frequency units.  Can not find combobox entry for: ' + self.frequency_units.name)
        self.cbFrequencyUnits.setCurrentIndex(cb_index)


    def get_center_frequency(self):
        try:
            center_frequency_hz = float(self.txtCenterFrequency.text()) * 10 ** (self.frequency_units.value * 3)
        except:
            center_frequency_hz = float('nan')
        return center_frequency_hz


    def set_center_frequency(self, center_frequency_hz, frequency_units=None):
        if frequency_units is None:
            frequency_units = self.get_frequency_units()
        self.txtCenterFrequency.setText(str(center_frequency_hz / 10.0 ** (frequency_units.value * 3)))


    def get_bandwidth(self):
        try:
            bandwidth_hz = float(self.txtBandwidth.text()) * 10 ** (self.frequency_units.value * 3)
        except:
            bandwidth_hz = float('nan')
        return bandwidth_hz


    def set_bandwidth(self, bandwidth_hz, frequency_units=None):
        if frequency_units is None:
            frequency_units = self.get_frequency_units()
        self.txtBandwidth.setText(str(bandwidth_hz / 10.0 ** (frequency_units.value * 3)))


    def get_start_time_sec(self):
        try:
            start_time_sec = float(self.txtStartTimeSec.text())
        except:
            start_time_sec = float('nan')
        return start_time_sec


    def set_start_time_sec(self, start_time_sec):
        self.txtStartTimeSec.setText(str(start_time_sec))


    def update_start_time_dtg(self, new_start_time_sec):
        try:
            new_start_time_dtg = self.original_start_time_dtg + np.timedelta64(int(1e9 * (float(new_start_time_sec) - self.original_start_time_sec)), 'ns')
            self.txtStartTimeDTG.setText(str(new_start_time_dtg).replace('T', ' '))
        except:
            self.txtStartTimeDTG.setText('-')
        self.txtStartTimeDTG.setCursorPosition(0)


    def set_start_time_dtg(self, start_time_dtg):
        self.start_time_dtg = start_time_dtg
        self.txtStartTimeDTG.setText(str(start_time_dtg).replace('T', ' '))


    def get_end_time_sec(self):
        try:
            end_time_sec = float(self.txtEndTimeSec.text())
        except:
            end_time_sec = float('nan')
        return end_time_sec


    def set_end_time_sec(self, end_time_sec):
        self.txtEndTimeSec.setText(str(end_time_sec))


    def get_num_taps(self):
        try:
            num_taps = int(self.txtNumTaps.text())
        except:
            num_taps = -1
        return num_taps
        self.txtNumTaps.setText(str(num_taps))


    def set_num_taps(self, num_taps):
        self.txtNumTaps.setText(str(num_taps))


if __name__=="__main__":
    import sys
    app = QApplication(sys.argv)
    dialog = SelectionAndProcessingParametersDialog(1000e6, 100e6, 0, 1, 271)
    dialog.show()
    sys.exit(app.exec_())
