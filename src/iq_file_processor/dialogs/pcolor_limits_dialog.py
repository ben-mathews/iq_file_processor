import os
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.uic import loadUi

qtCreatorFile = os.path.dirname(os.path.abspath(__file__)) + '/' + 'pcolor_limits_dialog.ui'

class PColorLimitsDialog(QDialog):
    def __init__(self, lower_limit_db=None, upper_limit_db=None):
        super(PColorLimitsDialog, self).__init__()
        loadUi(qtCreatorFile, self)

        if lower_limit_db is not None:
            self.txt_lower_limit_db.setText(str(lower_limit_db))
        else:
            self.txt_lower_limit_db.setText('')
        if upper_limit_db is not None:
            self.txt_upper_limit_db.setText(str(upper_limit_db))
        else:
            self.txt_upper_limit_db.setText('')

        self.btn_reset_lower_limit.clicked.connect(self.reset_lower_limit)
        self.btn_reset_upper_limit.clicked.connect(self.reset_upper_limit)


    def accept(self):
        super(PColorLimitsDialog, self).accept()
        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 1
        self.Rejected = 0

        if self.txt_lower_limit_db.text().replace('.','',1).replace('-','',1).isdigit():
            self.lower_limit = float(self.txt_lower_limit_db.text())
        else:
            self.lower_limit = None

        if self.txt_upper_limit_db.text().replace('.','',1).replace('-','',1).isdigit():
            self.upper_limit = float(self.txt_upper_limit_db.text())
        else:
            self.upper_limit = None


    def reject(self):
        super(PColorLimitsDialog, self).reject()
        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 0
        self.Rejected = 1


    def reset_lower_limit(self):
        self.txt_lower_limit_db.setText('')


    def reset_upper_limit(self):
        self.txt_upper_limit_db.setText('')


if __name__=="__main__":
    import sys
    app = QApplication(sys.argv)
    dialog = PColorLimitsDialog()
    dialog.show()
    sys.exit(app.exec_())
