import os
from PyQt5.QtWidgets import QApplication, QDialog, QMessageBox
from PyQt5.uic import loadUi

qtCreatorFile = os.path.dirname(os.path.abspath(__file__)) + '/' + 'transform_parameters_dialog.ui'

class SpectrogramParametersDialog(QDialog):
    def __init__(self, window_title, window, nperseg, noverlap, nfft, default_window, default_nperseg, default_noverlap, default_nfft):
        super(SpectrogramParametersDialog, self).__init__()
        loadUi(qtCreatorFile, self)

        self.default_window = default_window
        self.default_nperseg = default_nperseg
        self.default_noverlap = default_noverlap
        self.default_nfft = default_nfft

        self.pbResetToDefaults.clicked.connect(self.reset_to_defaults)

        self.setWindowTitle(window_title)
        cb_window_index = self.cbWindow.findText(window)
        if cb_window_index == -1:
            raise Exception('Can''t find matching combobox item for window = ' + window)
        self.cbWindow.setCurrentIndex(cb_window_index)
        self.txtNPerSeg.setText(str(int(nperseg)))
        self.txtNOverlap.setText(str(int(noverlap)))
        cb_nfft_index = self.cbNFFT.findText(str(nfft))
        if cb_nfft_index == -1:
            raise Exception('Can''t find matching combobox item for nfft = ' + window)
        self.cbNFFT.setCurrentIndex(cb_nfft_index)


    def accept(self):
        if int(self.cbNFFT.currentText()) < int(self.txtNPerSeg.text()):
            QMessageBox.warning(self, ' ', 'nfft must be greater than or equal to nperseg')
            return
        if int(self.txtNOverlap.text()) >= int(self.txtNPerSeg.text()):
            QMessageBox.warning(self, ' ', 'noverlap must be less than nperseg')
            return

        super(SpectrogramParametersDialog, self).accept()
        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 1
        self.Rejected = 0

        self.window = self.cbWindow.currentText()
        self.nperseg = int(self.txtNPerSeg.text())
        self.noverlap = int(self.txtNOverlap.text())
        self.nfft = int(self.cbNFFT.currentText())


    def reject(self):
        super(SpectrogramParametersDialog, self).reject()
        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 0
        self.Rejected = 1


    def reset_lower_limit(self):
        self.txt_lower_limit_db.setText('')


    def reset_upper_limit(self):
        self.txt_upper_limit_db.setText('')

    def reset_to_defaults(self):
        window = self.default_window
        nperseg = self.default_nperseg
        noverlap = self.default_noverlap
        nfft = self.default_nfft

        cb_window_index = self.cbWindow.findText(window)
        if cb_window_index == -1:
            raise Exception('Can''t find matching combobox item for window = ' + window)
        self.cbWindow.setCurrentIndex(cb_window_index)
        self.txtNPerSeg.setText(str(int(nperseg)))
        self.txtNOverlap.setText(str(int(noverlap)))
        cb_nfft_index = self.cbNFFT.findText(str(nfft))
        if cb_nfft_index == -1:
            raise Exception('Can''t find matching combobox item for nfft = ' + window)
        self.cbNFFT.setCurrentIndex(cb_nfft_index)



if __name__=="__main__":
    import sys
    app = QApplication(sys.argv)
    dialog = SpectrogramParametersDialog('Window Title', 'hann', 1024, 1024/8, 1024)
    dialog.show()
    sys.exit(app.exec_())
