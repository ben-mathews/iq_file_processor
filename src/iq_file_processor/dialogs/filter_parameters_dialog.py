import os
from PyQt5.QtWidgets import QApplication, QDialog, QMessageBox
from PyQt5.uic import loadUi

qtCreatorFile = os.path.dirname(os.path.abspath(__file__)) + '/' + 'filter_parameters_dialog.ui'

class FilterParametersDialog(QDialog):
    def __init__(self, window, numtaps, default_window, default_numtaps):
        super(FilterParametersDialog, self).__init__()
        loadUi(qtCreatorFile, self)

        self.default_window = default_window
        self.default_numtaps = default_numtaps

        self.pbResetToDefaults.clicked.connect(self.reset_to_defaults)

        cb_window_index = self.cbWindow.findText(window)
        if cb_window_index == -1:
            raise Exception('Can''t find matching combobox item for window = ' + window)
        self.cbWindow.setCurrentIndex(cb_window_index)
        self.txtNumTaps.setText(str(int(numtaps)))


    def accept(self):
        super(FilterParametersDialog, self).accept()
        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 1
        self.Rejected = 0

        self.window = self.cbWindow.currentText()
        self.numtaps = int(self.txtNumTaps.text())


    def reject(self):
        super(FilterParametersDialog, self).reject()
        # Manually setting Accepted and Rejected because PyQT doesn't seem to when calling accept() and reject() methods
        self.Accepted = 0
        self.Rejected = 1


    def reset_to_defaults(self):
        window = self.default_window
        numtaps = self.default_numtaps

        cb_window_index = self.cbWindow.findText(window)
        if cb_window_index == -1:
            raise Exception('Can''t find matching combobox item for window = ' + window)
        self.cbWindow.setCurrentIndex(cb_window_index)
        self.txtNumTaps.setText(str(int(numtaps)))



if __name__=="__main__":
    import sys
    app = QApplication(sys.argv)
    dialog = FilterParametersDialog('hamming', 271, 'hamming', 271)
    dialog.show()
    sys.exit(app.exec_())
