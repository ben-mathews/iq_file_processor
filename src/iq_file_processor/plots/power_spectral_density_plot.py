import math
import numpy as np
from scipy.signal import welch

import pyqtgraph
from pyqtgraph import GraphicsLayoutWidget
from PyQt5.QtWidgets import QAction, QMenu, QInputDialog

from iq_file_processor.scipy_parallel.scipy_parallel_implementations import welch_parallel


class PowerSpectralDensityPlot(GraphicsLayoutWidget):

    def __init__(self, iq, fs_hz, center_frequency, settings, me=None, signal_statusbar_text=None):
        super().__init__()

        self.iq = iq
        self.fs_hz = fs_hz
        self.center_frequency_hz = center_frequency
        self.settings = settings
        self.me = me
        self.plot_item = None
        self.frequency_span_selector = None
        self.f_welch = None
        self.s_welch = None
        self.signal_statusbar_text = signal_statusbar_text
        self.markers = []
        self.generate_plot()


    def __del__(self):
        super(PowerSpectralDensityPlot, self).__del__()
        self.iq = None
        self.f_welch = None
        self.s_welch = None
        self.plot_item = None


    def update_statusbar_text(self, statusbar_text):
        if self.signal_statusbar_text is not None:
            self.signal_statusbar_text.emit(statusbar_text)


    def clear_all_markers_callback(self):
        for marker in self.markers:
            self.plot_item.removeItem(marker)


    def click_to_add_new_marker_start(self):
        self.update_statusbar_text('Click to add new marker')
        self.enable_crosshairs()
        self.plot_item.plot().scene().sigMouseClicked.connect(self.click_to_add_new_marker_end)


    def click_to_add_new_marker_end(self, event):
        # TODO: Figure out how to use something like if not self.plot_item.plot().boundingRect().contains(event.scenePos()) instead of using try
        try:
            click_location = self.plot_item.vb.mapSceneToView(event.scenePos())
            frequency, amplitude = click_location.x(), click_location.y()
            frequency, amplitude = self.find_nearest_frequency_power(frequency)

            self.add_marker(amplitude, frequency)

            self.disable_crosshairs()
            self.plot_item.plot().scene().sigMouseClicked.disconnect(self.click_to_add_new_marker_end)
            self.update_statusbar_text('')

        except Exception as ex:
            do_nothing = 1

    def add_marker(self, amplitude, frequency):
        text = pyqtgraph.TextItem('{:3.3f} {}'.format(frequency, self.settings["frequency_units"].name),
                                  anchor=(0.0, 1))
        text.setPos(frequency, amplitude)
        self.plot_item.addItem(text)
        self.markers.append(text)
        arrow = pyqtgraph.ArrowItem(pos=(frequency, amplitude), angle=-90)
        self.plot_item.addItem(arrow)
        self.markers.append(arrow)


    def enable_crosshairs(self):
        self.plot_item.plot().scene().sigMouseMoved.connect(self.update_crosshairs)
        self.vLine = pyqtgraph.InfiniteLine(angle=90, movable=False)
        self.hLine = pyqtgraph.InfiniteLine(angle=0, movable=False)
        self.plot_item.addItem(self.vLine, ignoreBounds=True)
        self.plot_item.addItem(self.hLine, ignoreBounds=True)


    def update_crosshairs(self, event):
        if self.plot_item.sceneBoundingRect().contains(event):
            mousePoint = self.plot_item.vb.mapSceneToView(event)
            self.vLine.setPos(mousePoint.x())
            self.hLine.setPos(mousePoint.y())


    def disable_crosshairs(self):
        self.plot_item.removeItem(self.vLine)
        self.plot_item.removeItem(self.hLine)
        self.plot_item.plot().scene().sigMouseMoved.disconnect(self.update_crosshairs)


    def enter_frequency_to_add_new_marker_callback(self):
        frequency, ok_pressed = QInputDialog.getDouble(self, "Enter Marker Frequency", "Value {}:".format(self.settings["frequency_units"].name), np.mean([self.f_welch[0], self.f_welch[-1]]), self.f_welch[0], self.f_welch[-1], 1)
        if ok_pressed:
            frequency, amplitude = self.find_nearest_frequency_power(frequency)
            self.add_marker(amplitude, frequency)


    def find_max_amplitude_callback(self):
        if self.frequency_span_selector not in self.plot_item.items:
            self.add_frequency_span_selector()
            self.plot_item.getViewBox().actionAutoRange.setChecked(True)
        else:
            selected_frequencies_hz = self.get_frequency_span_selector_range()
            selected_frequencies_hz = selected_frequencies_hz / 10 ** (self.settings["frequency_units"].value * 3)
            selection_indicies = np.where((self.f_welch >= selected_frequencies_hz[0]) & (self.f_welch <= selected_frequencies_hz[1]))[0]
            max_index = np.argmax(self.s_welch[selection_indicies])
            s_welch_max = self.s_welch[selection_indicies[max_index]]
            f_welch_max = self.f_welch[selection_indicies[max_index]]
            print('Max amplitude {} dB at frequency {} {}'.format(s_welch_max, f_welch_max,
                                                                  self.settings["frequency_units"].name))

            self.add_marker(s_welch_max, f_welch_max)

            self.remove_frequency_span_selector()
            self.plot_item.getViewBox().actionAutoRange.setChecked(False)


    def generate_plot(self):
        if self.me is not None and hasattr(self.me, 'psd') and callable(getattr(self.me, 'psd', None)):
            self.f_welch, self.s_welch = self.me.psd(iq=self.iq, fs_hz=self.fs_hz, nfft=self.settings["psd"]["nfft"],
                                                     window='hann', center_frequency_hz=self.center_frequency_hz,
                                                     multi_threaded=self.settings["psd"]["do_multi_threaded"])
        else:
            if self.settings["psd"]["do_multi_threaded"]:
                self.f_welch, self.s_welch = welch_parallel(self.iq, self.fs_hz, 'hann', self.settings["psd"]["nfft"],
                                                            return_onesided=False, detrend=False, num_threads=4)
            else:
                self.f_welch, self.s_welch = welch(self.iq, self.fs_hz, 'hann', self.settings["psd"]["nfft"],
                                                   return_onesided=False, detrend=False)
            self.f_welch = self.f_welch / 10 ** (self.settings["frequency_units"].value * 3)
            self.f_welch = self.center_frequency_hz + np.fft.fftshift(self.f_welch)
            self.s_welch = np.fft.fftshift(10 * np.log10(self.s_welch))

        self.f_welch = self.f_welch / 10 ** (self.settings["frequency_units"].value * 3)

        self.plot_item = self.addPlot(row=0, col=0)

        # Update right-click menu
        viewBox = self.plot_item.getViewBox()

        # Move some built-in options to an Other Options submenu
        otherOptionsMenu = QMenu(title='Other Options', parent=viewBox.menu)
        for action in viewBox.menu.actions():
            otherOptionsMenu.addAction(action)
        for action in otherOptionsMenu.actions():
            if action in viewBox.menu.actions():
                viewBox.menu.removeAction(action)
        otherOptionsMenu.addMenu(self.plot_item.ctrlMenu)
        self.plot_item.ctrlMenu = None
        otherOptionsMenu.addAction(self.plot_item.scene().contextMenu[0])
        self.plot_item.scene().contextMenu = None

        markerMenu = QMenu(title='Markers', parent=viewBox.menu)
        viewBox.actionAutoRange = QAction("Clear All Markers", viewBox.menu)
        viewBox.actionAutoRange.triggered.connect(self.clear_all_markers_callback)
        markerMenu.addAction(viewBox.actionAutoRange)
        viewBox.actionAutoRange = QAction("Click to Add New Marker", viewBox.menu)
        viewBox.actionAutoRange.triggered.connect(self.click_to_add_new_marker_start)
        markerMenu.addAction(viewBox.actionAutoRange)
        viewBox.actionAutoRange = QAction("Enter Frequency to Add New Marker", viewBox.menu)
        viewBox.actionAutoRange.triggered.connect(self.enter_frequency_to_add_new_marker_callback)
        markerMenu.addAction(viewBox.actionAutoRange)
        viewBox.actionAutoRange = QAction("Peak Detect and Add Marker", viewBox.menu)
        viewBox.actionAutoRange.setCheckable(True)
        viewBox.actionAutoRange.triggered.connect(self.find_max_amplitude_callback)
        markerMenu.addAction(viewBox.actionAutoRange)

        viewBox.menu.addMenu(markerMenu)
        viewBox.menu.addMenu(otherOptionsMenu)

        self.plot_item.showGrid(x=True, y=True)
        self.plot_item.setLabel('left', 'Power (dB)')
        self.plot_item.setLabel('bottom', 'Frequency', units=self.settings["frequency_units"].name)
        self.plot_item.getAxis('left').enableAutoSIPrefix(False)
        self.plot_item.getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_item.setLimits(xMin=self.f_welch[0], xMax=self.f_welch[-1],
                                 yMin=np.min(self.s_welch) - np.abs(np.max(self.s_welch)), yMax=np.max(self.s_welch)+10)
        self.plot_item.plot(self.f_welch, self.s_welch, pen=pyqtgraph.mkColor("y"))
        self.pen_select_box_outline = pyqtgraph.mkColor("r")
        self.pen_select_box_fill = pyqtgraph.mkColor("b").setAlpha(50)
        self.frequency_span_selector = pyqtgraph.LinearRegionItem(
            values=(self.f_welch[int(0.25 * len(self.f_welch))], self.f_welch[int(0.75 * len(self.f_welch))]),
            bounds=(min(self.f_welch), max(self.f_welch)),
            orientation='vertical', brush=self.pen_select_box_fill, pen=self.pen_select_box_outline,
            hoverBrush=self.pen_select_box_fill, hoverPen=self.pen_select_box_outline, movable=True, swapMode='blovk')
        self.frequency_span_selector.setObjectName('psd_plot_frequency_span_selector')

        for line in self.frequency_span_selector.lines:
            line.pen.setWidth(2)

    def find_nearest_frequency_power(self, frequency):
        idx = np.searchsorted(self.f_welch, frequency, side="left")
        if idx > 0 and (idx == len(self.f_welch) or math.fabs(frequency - self.f_welch[idx - 1]) < math.fabs(frequency - self.f_welch[idx])):
            return self.f_welch[idx - 1], self.s_welch[idx - 1]
        else:
            return self.f_welch[idx], self.s_welch[idx]


    def remove_selectors(self):
        self.remove_frequency_span_selector()


    def add_frequency_span_selector(self):
        self.plot_item.addItem(self.frequency_span_selector)
        cf = np.mean(self.plot_item.getViewBox().state['viewRange'][0])
        bw = np.diff(self.plot_item.getViewBox().state['viewRange'][0])[0]
        self.frequency_span_selector.setRegion([cf - bw/4, cf + bw/4])


    def remove_frequency_span_selector(self):
        if self.plot_item is not None and self.frequency_span_selector is not None:
            self.plot_item.removeItem(self.frequency_span_selector)


    def get_frequency_span_selector_range(self):
        selected_frequencies_hz = np.asarray(
            [
                self.frequency_span_selector.boundingRect().left(),
                self.frequency_span_selector.boundingRect().right(),
            ]
        ) * 10 ** (self.settings["frequency_units"].value * 3)
        return selected_frequencies_hz

if __name__ == '__main__':
    from pyqtgraph.Qt import QtWidgets, sys
    from iq_file_processor.__main__ import FrequencyUnits

    nsamples = 10000
    fs_hz = 1e6
    t_sec = np.linspace(0, nsamples - 1, nsamples) / fs_hz
    iq = np.random.randn(nsamples) + 1j * np.random.randn(nsamples) \
         + np.exp(1j * 2 * np.pi * (fs_hz / 5.5) * t_sec) \
         + np.exp(1j * 2 * np.pi * (fs_hz / 4.5) * t_sec)

    center_frequency = 1e9
    settings = {"frequency_units": FrequencyUnits.MHz, 'psd': {'nfft': 8192, 'do_multi_threaded': False}}

    app = QtWidgets.QApplication(sys.argv)
    mw = QtWidgets.QMainWindow()
    mw.resize(800, 800)
    psd = PowerSpectralDensityPlot(iq=iq, fs_hz=fs_hz, center_frequency=center_frequency, settings=settings)
    mw.setCentralWidget(psd)
    mw.setWindowTitle('PowerSpectralDensityPlot Example')
    mw.show()
    sys.exit(QtWidgets.QApplication.exec_())
