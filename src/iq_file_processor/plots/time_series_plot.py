import numpy as np
import pyqtgraph
from pyqtgraph import GraphicsLayoutWidget

class TimeSeriesPlot(GraphicsLayoutWidget):

    def __init__(self, iq, t_sec, downsample_factor = None):
        super().__init__()

        if downsample_factor is None:
            self.downsample_factor = int(np.max([1, int(len(iq) / 50000)]))
        else:
            self.downsample_factor = downsample_factor

        self.iq = iq[::self.downsample_factor]
        self.t_sec = t_sec[::self.downsample_factor]
        self.plot_item = None
        self.time_span_selector = None
        self.generate_plot()


    def __del__(self):
        super(TimeSeriesPlot, self).__del__()
        self.iq = None
        self.t_sec = None
        self.plot_item = None


    def generate_plot(self):
        self.plot_item = self.addPlot(row=0, col=0)
        abs_iq = np.abs(self.iq)
        abs_iq_max = np.max(abs_iq)
        abs_iq_min = np.max(abs_iq)
        self.plot_item.showGrid(x=True, y=True)
        self.plot_item.setLabel('left', 'Amplitude')
        self.plot_item.setLabel('bottom', 'Time', units='Sec')
        self.plot_item.getAxis('left').enableAutoSIPrefix(False)
        self.plot_item.getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_item.setLimits(xMin=self.t_sec[0], xMax=self.t_sec[-1], yMin=abs_iq_min - abs_iq_max,
                                 yMax=4 * abs_iq_max + abs_iq_min)
        self.plot_item.plot(self.t_sec, abs_iq, pen=pyqtgraph.mkColor("y"))
        self.pen_select_box_outline = pyqtgraph.mkColor("r")
        self.pen_select_box_fill = pyqtgraph.mkColor("b").setAlpha(50)
        self.time_span_selector = pyqtgraph.LinearRegionItem(
            values=(self.t_sec[int(0.25 * len(self.t_sec))], self.t_sec[int(0.75 * len(self.t_sec))]),
            bounds=(min(self.t_sec), max(self.t_sec)),
            orientation='vertical', brush=self.pen_select_box_fill, pen=self.pen_select_box_outline,
            hoverBrush=self.pen_select_box_fill, hoverPen=self.pen_select_box_outline, movable=True, swapMode='blovk')
        self.time_span_selector.setObjectName('time_series_plot_time_span_selector')
        for line in self.time_span_selector.lines:
            line.pen.setWidth(2)


    def remove_selectors(self):
        self.remove_time_span_selector()


    def add_time_span_selector(self):
        self.plot_item.addItem(self.time_span_selector)


    def remove_time_span_selector(self):
        if self.plot_item is not None and self.time_span_selector is not None:
            self.plot_item.removeItem(self.time_span_selector)
