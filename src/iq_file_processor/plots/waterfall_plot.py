import numpy as np
from scipy.signal import spectrogram

import pyqtgraph
from pyqtgraph import GraphicsLayoutWidget
from PyQt5.QtCore import QRect

from iq_file_processor.scipy_parallel.scipy_parallel_implementations import spectrogram_parallel


class WaterfallPlot(GraphicsLayoutWidget):

    def __init__(self, iq, fs_hz, center_frequency, t_start_sec, settings):
        super().__init__()

        self.iq = iq
        self.fs_hz = fs_hz
        self.center_frequency_hz = center_frequency
        self.t_start_sec = t_start_sec
        self.settings = settings
        self.plot_item = None
        self.frequency_span_selector = None
        self.time_span_selector = None
        self.time_frequency_roi_selector = None
        self.generate_plot()


    def __del__(self):
        super(WaterfallPlot, self).__del__()
        self.iq = None
        self.plot_item = None
        self.waterfall_image_item = None
        self.waterfall_histogram_image_item = None

    def generate_plot(self):
        # import time
        if self.settings["waterfall"]["do_multi_threaded"] and len(self.iq) > 1000000:
            # start_time = time.time()
            f, t, sxx = spectrogram_parallel(self.iq, self.fs_hz, window=self.settings["waterfall"]["window"],
                                             nperseg=self.settings["waterfall"]["nperseg"],
                                             noverlap=self.settings["waterfall"]["noverlap"],
                                             nfft=self.settings["waterfall"]["nfft"], return_onesided=False, mode='psd',
                                             num_threads=8)
            # elapsed_time = time.time() - start_time
        else:
            # start_time = time.time()
            f, t, sxx = spectrogram(self.iq, self.fs_hz, window=self.settings["waterfall"]["window"],
                                    nperseg=self.settings["waterfall"]["nperseg"], noverlap=self.settings["waterfall"]["noverlap"],
                                    nfft=self.settings["waterfall"]["nfft"], return_onesided=False, mode='psd')
            # elapsed_time2 = time.time() - start_time
        sxx = np.fft.fftshift(sxx, 0)
        sxx = 10 * np.log10(sxx)
        f = np.fft.fftshift(f)
        f = f / 10 ** (self.settings["frequency_units"].value * 3)
        f = self.center_frequency_hz / 10 ** (self.settings["frequency_units"].value * 3) + f
        if self.settings["waterfall"]["clip_min_spectra"]:
            sxx = np.clip(sxx, np.percentile(sxx, 3), np.max(sxx))
        self.settings["waterfall"]["current_state"]["min_val"] = np.min(sxx)
        self.settings["waterfall"]["current_state"]["max_val"] = np.max(sxx)
        if self.settings["waterfall"]["min_val"] is not None:
            vmin = self.settings["waterfall"]["min_val"]
        else:
            vmin = self.settings["waterfall"]["current_state"]["min_val"]
        if self.settings["waterfall"]["max_val"] is not None:
            vmax = self.settings["waterfall"]["max_val"]
        else:
            vmax = self.settings["waterfall"]["current_state"]["max_val"]

        self.plot_item = self.addPlot(row=0, col=0)
        self.plot_item.setLabel('left', 'Time', units='Sec')
        self.plot_item.setLabel('bottom', 'Frequency', units=self.settings["frequency_units"].name)
        self.plot_item.getAxis('left').enableAutoSIPrefix(False)
        self.plot_item.getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_item.setLimits(xMin=f[0], xMax=f[-1], yMin=self.t_start_sec + t[0], yMax=self.t_start_sec + t[-1])
        self.waterfall_image_item = pyqtgraph.ImageItem(sxx, autoDownsample=False, useOpenGL=True)
        self.waterfall_image_item.scale((f[-1] - f[0]) / np.size(sxx, axis=0), (t[-1] - t[0]) / np.size(sxx, axis=1))
        self.waterfall_image_item.translate(-1 * f[0] / (f[0]-f[-1]) * np.shape(sxx)[0], self.t_start_sec / ((t[-1] - t[0]) / np.size(sxx, axis=1)))
        self.plot_item.addItem(self.waterfall_image_item)
        self.waterfall_histogram_image_item = pyqtgraph.HistogramLUTItem()
        self.waterfall_histogram_image_item.setImageItem(self.waterfall_image_item)
        self.waterfall_histogram_image_item.setLevels(vmin, vmax)
        self.waterfall_histogram_image_item.gradient.restoreState(
            {'mode': 'rgb',
             'ticks': [(0.5, (0, 182, 188, 255)),
                       (1.0, (246, 111, 0, 255)),
                       (0.0, (75, 0, 113, 255))]})
        self.waterfall_histogram_image_item.gradient.loadPreset("viridis")
        # self.waterfall_histogram_image_item.gradient.setOrientation('bottom')
        if self.settings["waterfall"]["show_histogram"]:
            self.addItem(self.waterfall_histogram_image_item, row=0, col=1)
        self.pen_select_box_outline = pyqtgraph.mkColor("r")
        self.pen_select_box_fill = pyqtgraph.mkColor("b").setAlpha(50)
        f_limits = (f[int(0.25 * len(f))], f[int(0.75 * len(f))])
        t_limits = self.t_start_sec + (t[int(0.25 * len(t))], t[int(0.75 * len(t))])
        self.frequency_span_selector = \
            pyqtgraph.LinearRegionItem(values=f_limits, orientation='vertical', brush=self.pen_select_box_fill,
                                       bounds=(min(f), max(f)),
                                       pen=self.pen_select_box_outline, hoverBrush=self.pen_select_box_fill,
                                       hoverPen=self.pen_select_box_outline, movable=True, swapMode='blovk')
        self.frequency_span_selector.setObjectName('waterfall_frequency_span_selector')
        for line in self.frequency_span_selector.lines:
            line.pen.setWidth(2)
        self.time_span_selector = \
            pyqtgraph.LinearRegionItem(values=t_limits, orientation='horizontal', brush=self.pen_select_box_fill,
                                       bounds=(min(t), max(t)),
                                       pen=self.pen_select_box_outline, hoverBrush=self.pen_select_box_fill,
                                       hoverPen=self.pen_select_box_outline, movable=True, swapMode='blovk')
        self.time_span_selector.setObjectName('waterfall_time_span_selector')
        for line in self.time_span_selector.lines:
            line.pen.setWidth(2)
        self.time_frequency_roi_selector = \
            pyqtgraph.ROI(pos=np.array([np.mean(f_limits), np.mean(t_limits)]) - np.array([np.diff(f_limits)[0], np.diff(t_limits)[0]]) / 2,
                          size=(np.diff(f)[0], np.diff(t)[0]), angle=0.0, invertible=False,
                          maxBounds=QRect(min(f_limits), max(t_limits), np.diff(f_limits)[0], np.diff(t_limits)[0]),
                          snapSize=1.0, scaleSnap=False, translateSnap=False, rotateSnap=False,
                          pen=self.pen_select_box_outline, movable=False, rotatable=False, resizable=False,
                          removable=False)
        self.time_frequency_roi_selector.setObjectName('waterfall_time_frequency_roi_selector')


    def remove_selectors(self):
        self.remove_time_span_selector()
        self.remove_frequency_span_selector()
        self.remove_time_frequency_roi_selector()


    def add_time_span_selector(self):
        self.plot_item.addItem(self.time_span_selector)


    def remove_time_span_selector(self):
        if self.plot_item is not None and self.time_span_selector is not None:
            self.plot_item.removeItem(self.time_span_selector)


    def add_time_frequency_roi_selector(self):
        self.plot_item.addItem(self.time_frequency_roi_selector)


    def remove_time_frequency_roi_selector(self):
        if self.plot_item is not None and self.time_frequency_roi_selector is not None:
            self.plot_item.removeItem(self.time_frequency_roi_selector)


    def add_frequency_span_selector(self):
        self.plot_item.addItem(self.frequency_span_selector)


    def remove_frequency_span_selector(self):
        if self.plot_item is not None and self.frequency_span_selector is not None:
            self.plot_item.removeItem(self.frequency_span_selector)
