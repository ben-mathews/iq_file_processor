#!/usr/bin/env python
"""
IQ File Processor
"""

import os
import sys
import copy
from enum import Enum

from PyQt5 import QtWidgets, QtCore, QtGui, uic
from PyQt5.QtWidgets import QInputDialog, QAction
from PyQt5.QtGui import QPalette
from PyQt5.QtCore import pyqtSignal, pyqtSlot

import pyqtgraph

import numpy as np

from iq_file_processor.dialogs.file_selection_dialog import FileSelectionDialog
from iq_file_processor.dialogs.pcolor_limits_dialog import PColorLimitsDialog
from iq_file_processor.dialogs.selection_and_processing_parameters_dialog import SelectionAndProcessingParametersDialog
from iq_file_processor.dialogs.transform_parameters_dialog import SpectrogramParametersDialog
from iq_file_processor.dialogs.filter_parameters_dialog import FilterParametersDialog
from iq_file_processor.dialogs.variable_mapping_dialog import VariableMappingDialog
from iq_file_processor.plots.time_series_plot import TimeSeriesPlot
from iq_file_processor.plots.power_spectral_density_plot import PowerSpectralDensityPlot
from iq_file_processor.plots.waterfall_plot import WaterfallPlot

QT_CREATOR_FILE = os.path.dirname(os.path.abspath(__file__)) + "/" + "iq_file_processor_gui.ui"
UI_MAIN_WINDOW, QT_BASE_CLASS = uic.loadUiType(QT_CREATOR_FILE)

# sudo apt-get install python3-pyqt5 python3-pyqt5.qtsql qttools5-dev-tools qttools5-dev qt5-default

# TODO: Filter design dialog
# TODO: Add support for npy and npz files
# TODO: FSD not correctly detecting BF type when specifying tmp filename


class FrequencyUnits(Enum):
    """
    FrequencyUnits enumeration
    """

    Hz = 0
    KHz = 1
    MHz = 2
    GHz = 3


class IQFileProcessorMainWindow(QtWidgets.QMainWindow, UI_MAIN_WINDOW):
    """
    The IQFileProcessorMainWindow objects contains Time Series, PSD, and Waterfall windows and associated functionality
    for manipulating IQ data
    """

    signal_statusbar_text = pyqtSignal(str)

    def __init__(self):
        """
        Constructor for IQFileProcessorMainWindow objects
        """
        #
        # Basic set-up
        #
        QtWidgets.QMainWindow.__init__(self)
        UI_MAIN_WINDOW.__init__(self)
        self.setupUi(self)
        self.setMouseTracking(True)

        self.iq_file = None

        #
        # Initialize widgets
        #
        self.time_series_plot_widget = None
        self.psd_plot_widget = None
        self.waterfall_plot_widget = None
        self.recent_file_actions = []
        self.graphics_layout_widgets = []
        self.time_series_layout_widget = None
        self.time_series_plot_span_selector = None
        self.psd_layout_widget = None
        self.psd_plot_span_selector = None
        self.waterfall_layout_widget = None
        self.waterfall_time_span_selector = None
        self.waterfall_frequency_span_selector = None
        self.waterfall_roi_selector = None

        #
        # Settings
        #
        self.__initialize_settings__()

        #
        # Status
        #
        self.status = {}
        self.status["last_selection"] = {}
        self.status["last_selection"]["state"] = None
        self.status["state"] = "None"
        self.status["waterfall_time_frequency_roi_selector_signal_connected"] = True
        self.status["span_selectors_signal_connected"] = True
        self.status["selected_frequencies_hz"] = None
        self.__update_view_menu_checkboxes__()
        self.__update_button_states__()

        #
        # Configure callbacks
        #
        self.actionImport.triggered.connect(self.__menu_select_import_callback__)
        self.actionExit.triggered.connect(self.__menu_exit_callback__)
        self.actionFrequency_Units.triggered.connect(self.__menu_settings_frequency_units_callback__)
        self.actionPSD_Parameters.triggered.connect(self.__menu_settings_psd_parameters_callback__)
        self.actionWaterfall_Parameters.triggered.connect(self.__menu_view_waterfall_parameters_callback__)
        self.actionWaterfall_Color_Limits.triggered.connect(self.__menu_settings_pcolor_limits_callback__)
        self.actionShow_Waterfall_Colorbar.triggered.connect(self.__menu_settings_show_waterfall_colorbar_callback__)
        self.actionFilter_Parameters.triggered.connect(self.__menu_settings_filter_parameters_callback__)
        self.actionVariable_Mapping.triggered.connect(self.__menu_settings_variable_mapping_callback__)
        self.actionTime_Series_View.triggered.connect(self.__menu_view_time_series_plot_callback__)
        self.actionPower_Spectral_Density_Plot_View.triggered.connect(self.__menu_view_psd_plot_callback__)
        self.actionWaterfall_Plot_View.triggered.connect(self.__menu_view_waterfall_plot_callback__)
        self.cbFilterType.currentIndexChanged.connect(self.__cb_filter_type_changed_callback__)
        self.btnSelect.clicked.connect(self.__btn_select_callback__)
        self.btnUpdate.clicked.connect(self.__btn_update_callback__)
        self.btnExport.clicked.connect(self.__btn_export_callback__)
        self.btnReset.clicked.connect(self.__btn_reset_callback__)
        self.splitter.splitterMoved.connect(self.splitter_moved_callback)

        self.signal_statusbar_text.connect(self.update_statusbar_text)

        #
        # Get started
        #
        if not self.settings["recent_files"]["recent_file_list"]:
            self.import_file()
        else:
            self.import_file(filename=self.settings["recent_files"]["recent_file_list"][0])


    @QtCore.pyqtSlot(str)
    def update_statusbar_text(self, statusbar_text):
        self.statusBar().showMessage(statusbar_text)
        self.raise_()


    def __initialize_settings__(self):
        """
        Initialize the app to some default settings

        :return: void
        """
        self.settings = {}
        self.settings["time_series"] = {}
        self.settings["time_series"]["default_parameters"] = {}
        self.settings["time_series"]["default_parameters"]["show_time_series_plot"] = True
        self.settings["psd"] = {}
        self.settings["psd"]["default_parameters"] = {}
        self.settings["psd"]["default_parameters"]["show_psd_plot"] = True
        self.settings["psd"]["default_parameters"]["do_multi_threaded"] = False
        self.settings["psd"]["default_parameters"]["window"] = "hann"
        self.settings["psd"]["default_parameters"]["nperseg"] = 4096
        self.settings["psd"]["default_parameters"]["noverlap"] = 512
        self.settings["psd"]["default_parameters"]["nfft"] = 4096
        self.settings["waterfall"] = {}
        self.settings["waterfall"]["default_parameters"] = {}
        self.settings["waterfall"]["default_parameters"]["show_waterfall_plot"] = True
        self.settings["waterfall"]["default_parameters"]["do_multi_threaded"] = False
        self.settings["waterfall"]["default_parameters"]["window"] = "hann"
        self.settings["waterfall"]["default_parameters"]["nperseg"] = 1024
        self.settings["waterfall"]["default_parameters"]["noverlap"] = 256
        self.settings["waterfall"]["default_parameters"]["nfft"] = 1024
        self.settings["waterfall"]["current_state"] = {}
        self.settings["waterfall"]["current_state"]["min_val"] = None
        self.settings["waterfall"]["current_state"]["max_val"] = None
        self.settings["waterfall"]["min_val"] = None
        self.settings["waterfall"]["max_val"] = None
        self.settings["filter"] = {}
        self.settings["filter"]["window"] = 'hamming'
        self.settings["filter"]["numtaps"] = 291
        self.settings["recent_files"] = {}
        self.settings["recent_files"]["max_num"] = 4
        self.settings["variable_mapping"] = {
            'iq': 'iq',
            'fs_hz': 'fs_hz',
            't_start_sec': 't_start_sec',
            'center_frequency_hz': 'center_frequency_hz'
        }
        self.load_settings()
        self.update_recent_file_actions()


    def closeEvent(self, event):  # pylint: disable=C0103
        """
        Overloaded closeEvent() method
        :param event:
        :return: void

        Note: See https://doc.qt.io/qt-5/qwidget.html#closeEvent
        """
        self.save_settings()
        return super(IQFileProcessorMainWindow, self).closeEvent(event)


    def resizeEvent(self, QResizeEvent):  # pylint: disable=C0103
        """
        Overloaded closeEvent() method
        :param QResizeEvent: void
        :return:

        Note: See https://doc.qt.io/qt-5/qwidget.html#resizeEvent
        """
        self.__redraw_figures__()
        return super(IQFileProcessorMainWindow, self).resizeEvent(QResizeEvent)


    def splitter_moved_callback(self):
        """
        Callback for when the splitter inside of centralwidget is moved
        :return: void
        """
        self.__redraw_figures__()


    def load_settings(self):
        """
        Loads settings from Qsettings file into the IQFileProcessorMainWindow settings container

        :return: void
        """
        settings = QtCore.QSettings()
        if settings.value("frequency_units", "MHz", str) in FrequencyUnits.__dict__["_member_names_"]:
            self.settings["frequency_units"] = FrequencyUnits(
                FrequencyUnits.__dict__["_member_names_"].index(settings.value("frequency_units", "MHz", str))
            )
        else:
            self.settings["frequency_units"] = FrequencyUnits.MHz
        self.settings["time_series"]["show_time_series_plot"] = settings.value("time_series/show_time_series_plot",
                                                                               True, bool)
        self.settings["psd"]["show_psd_plot"] = settings.value("psd/show_psd_plot", True, bool)
        self.settings["psd"]["do_multi_threaded"] = settings.value("psd/do_multi_threaded", True, bool)
        self.settings["psd"]["window"] = settings.value("psd/window", "hann", str)
        self.settings["psd"]["nperseg"] = settings.value("psd/nperseg", 4096, int)
        self.settings["psd"]["noverlap"] = settings.value("psd/noverlap", 512, int)
        self.settings["psd"]["nfft"] = settings.value("psd/nfft", 4096, int)
        self.settings["waterfall"]["show_waterfall_plot"] = settings.value("waterfall/show_waterfall_plot", True, bool)
        self.settings["waterfall"]["do_multi_threaded"] = settings.value("waterfall/do_multi_threaded", True, bool)
        self.settings["waterfall"]["window"] = settings.value("waterfall/window", "hann", str)
        self.settings["waterfall"]["nperseg"] = settings.value("waterfall/nperseg", 1024, int)
        self.settings["waterfall"]["noverlap"] = settings.value("waterfall/noverlap", 128, int)
        self.settings["waterfall"]["nfft"] = settings.value("waterfall/nfft", 1024, int)
        self.settings["waterfall"]["clip_min_spectra"] = settings.value("waterfall/clip_min_spectra", True, bool)
        self.settings["waterfall"]["show_histogram"] = settings.value("waterfall/show_histogram", False, bool)
        self.settings["filter"]["window"] = settings.value("filter/window", 'hamming', str)
        self.settings["filter"]["numtaps"] = settings.value("filter/numtaps", 291, int)
        self.settings["recent_files"]["recent_file_list"] = settings.value("recent_file_list", [], list)
        self.settings["variable_mapping"] = settings.value("variable_mapping", [], dict)


    def save_settings(self):
        """
        Saves settings from the IQFileProcessorMainWindow settings container into the Qsettings file

        :return: void
        """
        settings = QtCore.QSettings()
        settings.setValue("frequency_units", self.settings["frequency_units"].value)
        settings.setValue("time_series/show_time_series_plot", self.settings["time_series"]["show_time_series_plot"])
        settings.setValue("psd/show_psd_plot", self.settings["psd"]["show_psd_plot"])
        settings.setValue("psd/nfft", self.settings["psd"]["nfft"])
        settings.setValue("psd/do_multi_threaded", self.settings["psd"]["do_multi_threaded"])
        settings.setValue("psd/window", self.settings["psd"]["window"])
        settings.setValue("psd/nperseg", self.settings["psd"]["nperseg"])
        settings.setValue("psd/noverlap", self.settings["psd"]["noverlap"])
        settings.setValue("psd/nfft", self.settings["psd"]["nfft"])
        settings.setValue("waterfall/show_waterfall_plot", self.settings["waterfall"]["show_waterfall_plot"])
        settings.setValue("waterfall/do_multi_threaded", self.settings["waterfall"]["do_multi_threaded"])
        settings.setValue("waterfall/window", self.settings["waterfall"]["window"])
        settings.setValue("waterfall/nperseg", self.settings["waterfall"]["nperseg"])
        settings.setValue("waterfall/noverlap", self.settings["waterfall"]["noverlap"])
        settings.setValue("waterfall/nfft", self.settings["waterfall"]["nfft"])
        settings.setValue("waterfall/clip_min_spectra", self.settings["waterfall"]["clip_min_spectra"])
        settings.setValue("waterfall/min_val", self.settings["waterfall"]["min_val"])
        settings.setValue("waterfall/max_val", self.settings["waterfall"]["max_val"])
        settings.setValue("waterfall/show_histogram", self.settings["waterfall"]["show_histogram"])
        settings.setValue("filter/window", self.settings["filter"]["window"])
        settings.setValue("filter/numtaps", self.settings["filter"]["numtaps"])
        settings.setValue('recent_file_list', self.settings["recent_files"]["recent_file_list"])
        settings.setValue('variable_mapping', self.settings["variable_mapping"])


    def update_recent_file_actions(self):
        """
        Updates the "Recent Files" submenu

        :return: void
        """
        for action in self.menuImport_Recent.actions():
            self.menuImport_Recent.removeAction(action)
        if self.settings["recent_files"]["recent_file_list"] is not None:
            num_recent_files = min(
                len(self.settings["recent_files"]["recent_file_list"]), self.settings["recent_files"]["max_num"]
            )
            self.recent_file_actions = []
            for filename in self.settings["recent_files"]["recent_file_list"][0:num_recent_files]:
                self.recent_file_actions.append(QAction(self, visible=True, triggered=self.__open_recent_file__))
                self.recent_file_actions[-1].setText(filename)
                self.recent_file_actions[-1].setData(filename)
                self.menuImport_Recent.addAction(self.recent_file_actions[-1])


    #
    # ----------------------------------------- Begin Menubar Callbacks -----------------------------------------
    #


    def __menu_select_import_callback__(self):
        """
        Callback for when user enters File->Import menu

        :return: void
        """
        self.import_file()


    def __open_recent_file__(self):
        """
        Opens a file from the "Recent Files" submenu

        :return: void
        """
        action = self.sender()
        if action:
            if os.path.isfile(action.data()):
                self.import_file(action.data())
            else:
                self.menuImport_Recent.removeAction(action)


    def __menu_exit_callback__(self):
        """
        Callback for when user enters File->Import menu

        :return: void
        """
        self.close()


    def __menu_settings_frequency_units_callback__(self):
        """
        Callback for when user enters Settings->Frequency Units menu

        :return: void
        """
        items = ("Hz", "KHz", "MHz", "GHz")
        item, ok_pressed = QInputDialog.getItem(self, "Select Frequency Units", "Units:", items, 0, False)
        if ok_pressed and item:
            new_frequency_units = [val for val in FrequencyUnits if val.name == item][0]
            if self.settings["frequency_units"] != new_frequency_units:
                self.settings["frequency_units"] = new_frequency_units
                self.update_plots()


    def __menu_settings_psd_parameters_callback__(self):
        """
        Callback for when user enters Settings->PSD Parameters menu

        :return: void
        """
        spectrogram_params_dialog = SpectrogramParametersDialog(
            "PSD Parameters",
            self.settings["psd"]["window"],
            self.settings["psd"]["nperseg"],
            self.settings["psd"]["noverlap"],
            self.settings["psd"]["nfft"],
            self.settings["psd"]["default_parameters"]["window"],
            self.settings["psd"]["default_parameters"]["nperseg"],
            self.settings["psd"]["default_parameters"]["noverlap"],
            self.settings["psd"]["default_parameters"]["nfft"],
        )
        spectrogram_params_dialog.exec_()
        if spectrogram_params_dialog.Accepted:
            self.settings["psd"]["window"] = spectrogram_params_dialog["window"]
            self.settings["psd"]["nperseg"] = spectrogram_params_dialog["nperseg"]
            self.settings["psd"]["noverlap"] = spectrogram_params_dialog["noverlap"]
            self.settings["psd"]["nfft"] = spectrogram_params_dialog["nfft"]
            self.update_plots()


    def __menu_view_waterfall_parameters_callback__(self):
        """
        Callback for when user enters Settings->Waterfall Parameters menu

        :return: void
        """
        spectrogram_params_dialog = SpectrogramParametersDialog(
            "Waterfall Parameters",
            self.settings["waterfall"]["window"],
            self.settings["waterfall"]["nperseg"],
            self.settings["waterfall"]["noverlap"],
            self.settings["waterfall"]["nfft"],
            self.settings["waterfall"]["default_parameters"]["window"],
            self.settings["waterfall"]["default_parameters"]["nperseg"],
            self.settings["waterfall"]["default_parameters"]["noverlap"],
            self.settings["waterfall"]["default_parameters"]["nfft"],
        )
        spectrogram_params_dialog.exec_()
        if spectrogram_params_dialog.Accepted:
            self.settings["waterfall"]["window"] = spectrogram_params_dialog["window"]
            self.settings["waterfall"]["nperseg"] = spectrogram_params_dialog["nperseg"]
            self.settings["waterfall"]["noverlap"] = spectrogram_params_dialog["noverlap"]
            self.settings["waterfall"]["nfft"] = spectrogram_params_dialog["nfft"]
            self.update_plots()


    def __menu_settings_pcolor_limits_callback__(self):
        """
        Callback for when user enters Settings->Waterfall Color Limits menu

        :return: void
        """
        if self.settings["waterfall"]["min_val"] is not None:
            vmin = self.settings["waterfall"]["min_val"]
        else:
            vmin = self.settings["waterfall"]["current_state"]["min_val"]
        if self.settings["waterfall"]["max_val"] is not None:
            vmax = self.settings["waterfall"]["max_val"]
        else:
            vmax = self.settings["waterfall"]["current_state"]["max_val"]

        pcolor_limit_dialog = PColorLimitsDialog(vmin, vmax)
        pcolor_limit_dialog.exec_()

        if pcolor_limit_dialog.Accepted:
            self.settings["waterfall"]["min_val"] = pcolor_limit_dialog.lower_limit
            self.settings["waterfall"]["max_val"] = pcolor_limit_dialog.upper_limit
            self.update_plots()


    def __menu_settings_show_waterfall_colorbar_callback__(self):
        """
        Callback for when user enters Settings->Show Waterfall Colorbar menu

        :return: void
        """
        items = ("Yes", "No")
        item, ok_pressed = QInputDialog.getItem(self, "Show Colorbar", "Show Colorbar:", items, 0, False)
        if ok_pressed and item:
            if item == "Yes":
                self.actionShow_Waterfall_Colorbar.setChecked(True)
                if not self.settings["waterfall"]["show_histogram"]:
                    self.settings["waterfall"]["show_histogram"] = True
                    self.update_plots()
            elif item == "No":
                self.actionShow_Waterfall_Colorbar.setChecked(False)
                if self.settings["waterfall"]["show_histogram"]:
                    self.settings["waterfall"]["show_histogram"] = False
                    self.update_plots()


    def __menu_settings_filter_parameters_callback__(self):
        """
        Callback for when user enters Settings->Filter Parameters menu

        :return:
        """
        filter_parameters_dialog = FilterParametersDialog(window=self.settings["filter"]["window"],
                                                          numtaps=self.settings["filter"]["numtaps"],
                                                          default_window='hamming', default_numtaps=271)

        filter_parameters_dialog.exec_()
        if filter_parameters_dialog.Accepted:
            self.settings["filter"]["window"] = filter_parameters_dialog.window
            self.settings["filter"]["numtaps"] = filter_parameters_dialog.numtaps


    def __menu_settings_variable_mapping_callback__(self):
        variable_mapping_dialog = VariableMappingDialog(variable_mapping=self.settings["variable_mapping"])
        variable_mapping_dialog.exec_()
        self.settings["variable_mapping"] = variable_mapping_dialog.variable_mapping


    def __menu_view_time_series_plot_callback__(self):
        """
        Callback for when user enters View->Time Series Plot menu

        :return: void
        """
        self.settings["time_series"]["show_time_series_plot"] = \
            not self.settings["time_series"]["show_time_series_plot"]
        self.__update_view_menu_checkboxes__()
        self.update_plots()


    def __menu_view_psd_plot_callback__(self):
        """
        Callback for when user enters View->Power Spectral Density Plot menu

        :return: void
        """
        self.settings["psd"]["show_psd_plot"] = not self.settings["psd"]["show_psd_plot"]
        self.__update_view_menu_checkboxes__()
        self.update_plots()


    def __menu_view_waterfall_plot_callback__(self):
        """
        Callback for when user enters View->Waterfall Plot menu

        :return: void
        """
        self.settings["waterfall"]["show_waterfall_plot"] = not self.settings["waterfall"]["show_waterfall_plot"]
        self.__update_view_menu_checkboxes__()
        self.update_plots()


    def __update_view_menu_checkboxes__(self):
        """
        Update checkboxes on the entries on the View menu that correspond to plots that may be shown or hidden

        :return: void
        """
        self.actionTime_Series_View.setChecked(self.settings["time_series"]["show_time_series_plot"])
        self.actionPower_Spectral_Density_Plot_View.setChecked(self.settings["psd"]["show_psd_plot"])
        self.actionWaterfall_Plot_View.setChecked(self.settings["waterfall"]["show_waterfall_plot"])


    #
    # ------------------------------------------ End Menubar Callbacks ------------------------------------------
    #


    #
    # ----------------------------------- Begin Combobox and Button Callbacks -----------------------------------
    #

    def __cb_filter_type_changed_callback__(self):
        """
        Called when the filter type combo box changes
        :return:
        """
        self.remove_plot_widget_selectors()
        if self.cbFilterType.currentText() == "None":
            self.__btn_select_callback__()
        if self.status["state"] != "None" and self.cbFilterType.currentText() != "None":
            self.status["state"] = "None"
        self.__update_button_states__()


    def __btn_select_callback__(self):
        """
        Callback function for selecting data to process
        :return:
        """
        # Reset selectors
        self.remove_plot_widget_selectors()

        # Activate the proper span or rectangle selector
        if self.cbFilterType.currentText() == "None":
            self.status["state"] = "None"
        elif self.cbFilterType.currentText() == "Time+Frequency":
            if self.time_series_layout_widget is not None:
                self.time_series_layout_widget.add_time_span_selector()
            if self.psd_layout_widget is not None:
                self.psd_layout_widget.add_frequency_span_selector()
            if self.waterfall_layout_widget is not None:
                self.waterfall_layout_widget.add_time_frequency_roi_selector()
            self.status["state"] = "Selecting Time+Frequency Box"
            self.update_selected_frequencies_hz()
        elif self.cbFilterType.currentText() == "Frequency Only":
            if self.psd_layout_widget is not None:
                self.psd_layout_widget.add_frequency_span_selector()
            if self.waterfall_layout_widget is not None:
                self.waterfall_layout_widget.add_frequency_span_selector()
            self.status["state"] = "Selecting Frequency Span"
            self.update_selected_frequencies_hz()
        elif self.cbFilterType.currentText() == "Time Only":
            if self.time_series_layout_widget is not None:
                self.time_series_layout_widget.add_time_span_selector()
            if self.waterfall_layout_widget is not None:
                self.waterfall_layout_widget.add_time_span_selector()
            self.status["state"] = "Selecting Time Span"
        else:
            raise Exception("Expecting status state to be None")
        self.__update_button_states__()


    def __btn_update_callback__(self):
        """
        Callback function for when user hits Update button
        :return: void
        """
        self.status["last_selection"]["state"] = self.status["state"]
        bandwidth_hz, center_frequency_hz, end_time_sec, start_time_sec, start_time_dtg = \
            self.__get_selection_parameters__()

        sappd = SelectionAndProcessingParametersDialog(
            center_frequency_hz=center_frequency_hz,
            bandwidth_hz=bandwidth_hz,
            start_time_sec=start_time_sec,
            start_time_dtg=start_time_dtg,
            end_time_sec=end_time_sec,
            num_taps=self.settings["filter"]["numtaps"],
        )
        sappd.exec_()
        if sappd.Accepted:
            bandwidth_hz = sappd.bandwidth_hz
            center_frequency_hz = sappd.center_frequency_hz
            start_time_sec = sappd.start_time_sec
            start_time_dtg = sappd.start_time_dtg
            end_time_sec = sappd.end_time_sec
        else:
            return


        if self.status["last_selection"]["state"] == 'Selecting Time+Frequency Box':
            self.iq_file.t_iq_sec, self.iq_file.iq = self.iq_file.time_filter(
                start_time_sec=start_time_sec, end_time_sec=end_time_sec
            )
            self.iq_file.update_start_time_sec(t_start_sec=self.iq_file.t_iq_sec[0])
            self.iq_file.update_num_samples(num_samples=len(self.iq_file.iq))
            self.iq_file.iq = self.iq_file.tune(center_frequency_hz-self.iq_file.center_frequency_hz)
            self.iq_file.iq = self.iq_file.low_pass_filter(bandwidth_hz)
            self.iq_file.iq, resample_frequency_frac = self.iq_file.resample(bandwidth_hz, denominator_limit=1000)
            resample_frequency = (
                self.iq_file.fs_hz * resample_frequency_frac.numerator / resample_frequency_frac.denominator
            )
            self.iq_file.update_num_samples(len(self.iq_file.iq))
            self.iq_file.update_sample_rate(resample_frequency)
        elif self.status["last_selection"]["state"] == "Selecting Frequency Span":
            self.iq_file.iq = self.iq_file.tune(center_frequency_hz-self.iq_file.center_frequency_hz)
            self.iq_file.iq = self.iq_file.low_pass_filter(cutoff_frequency_hz=bandwidth_hz,
                                                           numtaps=self.settings["filter"]["numtaps"],
                                                           window=self.settings["filter"]["window"])
            self.iq_file.iq, resample_frequency_frac = self.iq_file.resample(bandwidth_hz, denominator_limit=1000)
            resample_frequency = (
                self.iq_file.fs_hz * resample_frequency_frac.numerator / resample_frequency_frac.denominator
            )
            self.iq_file.update_num_samples(len(self.iq_file.iq))
            self.iq_file.update_sample_rate(resample_frequency)
        elif self.status["last_selection"]["state"] == "Selecting Time Span":
            self.iq_file.t_iq_sec, self.iq_file.iq = self.iq_file.time_filter(
                start_time_sec=start_time_sec, end_time_sec=end_time_sec
            )
            self.iq_file.update_start_time_sec(t_start_sec=self.iq_file.t_iq_sec[0])
            self.iq_file.update_num_samples(num_samples=len(self.iq_file.iq))
        elif self.status["last_selection"]["state"] == "None":
            self.iq_file = self.iq_file
        else:
            raise Exception('Unexpected self.status["state"]')

        self.status["state"] = "None"
        self.status["last_selection"]["state"] = "None"
        self.cbFilterType.setCurrentIndex(self.cbFilterType.findText("None"))
        self.update_plots()


    def __btn_export_callback__(self):
        """
        Callback function for when user hits Export button
        :return: void
        """
        self.status["last_selection"]["state"] = self.status["state"]
        bandwidth_hz, center_frequency_hz, end_time_sec, start_time_sec, start_time_dtg = self.__get_selection_parameters__()
        sappd = SelectionAndProcessingParametersDialog(
            center_frequency_hz=center_frequency_hz,
            bandwidth_hz=bandwidth_hz,
            start_time_sec=start_time_sec,
            start_time_dtg=start_time_dtg,
            end_time_sec=end_time_sec,
            num_taps=self.settings["filter"]["numtaps"],
        )
        sappd.exec_()
        if sappd.Accepted:
            bandwidth_hz = sappd.bandwidth_hz
            center_frequency_hz = sappd.center_frequency_hz
            start_time_sec = sappd.start_time_sec
            start_time_dtg = sappd.start_time_dtg
            end_time_sec = sappd.end_time_sec
        else:
            return

        fsd = FileSelectionDialog(selection_mode="export", iq_file=self.iq_file,
                                  center_frequency_hz=center_frequency_hz, fs_hz=bandwidth_hz)
        fsd.exec_()
        if fsd.Accepted:
            iq_file = fsd.results.iq_file
            iq_file.iq = None
            iq_file.t_iq_sec = None
            iq_file.update_sample_rate(self.iq_file.fs_hz)
            if self.status["last_selection"]["state"] == "Selecting Time+Frequency Box":
                iq_file.t_iq_sec, iq_file.iq = self.iq_file.time_filter(start_time_sec=start_time_sec,
                                                                        end_time_sec=end_time_sec)
                iq_file.update_start_time_sec(t_start_sec=iq_file.t_iq_sec[0])
                iq_file.update_start_time_dtg(start_time_dtg=start_time_dtg)
                iq_file.update_num_samples(num_samples=len(iq_file.iq))
                iq_file.iq = iq_file.tune(center_frequency_hz-self.iq_file.center_frequency_hz)
                iq_file.iq = iq_file.low_pass_filter(bandwidth_hz)
                iq_file.iq, resample_frequency_frac = iq_file.resample(bandwidth_hz)
                resample_frequency = (
                    iq_file.fs_hz * resample_frequency_frac.numerator / resample_frequency_frac.denominator
                )
                iq_file.update_num_samples(len(iq_file.iq))
                iq_file.update_sample_rate(resample_frequency)
            elif self.status["last_selection"]["state"] == "Selecting Frequency Span":
                iq_file.iq = self.iq_file.tune(center_frequency_hz-self.iq_file.center_frequency_hz)  # iq_file.t gets generated when we do this
                iq_file.iq = iq_file.low_pass_filter(bandwidth_hz)
                iq_file.iq, resample_frequency_frac = iq_file.resample(bandwidth_hz)
                resample_frequency = (
                    iq_file.fs_hz * resample_frequency_frac.numerator / resample_frequency_frac.denominator
                )
                iq_file.update_center_frequency_hz(center_frequency_hz=center_frequency_hz)
                iq_file.update_num_samples(len(iq_file.iq))
                iq_file.update_sample_rate(resample_frequency)
            elif self.status["last_selection"]["state"] == "Selecting Time Span":
                iq_file.t_iq_sec, iq_file.iq = self.iq_file.time_filter(start_time_sec=start_time_sec,
                                                                        end_time_sec=end_time_sec)
                iq_file.update_start_time_sec(t_start_sec=iq_file.t_iq_sec[0])

                iq_file.update_start_time_dtg(
                    start_time_dtg=self.iq_file.timecode_datetime + np.timedelta64(int(1e9*(iq_file.t_iq_sec[0] -
                                                                                            self.iq_file.t_iq_sec[0])),
                                                                                   'ns'))
                iq_file.update_num_samples(num_samples=len(iq_file.iq))
            elif self.status["last_selection"]["state"] == "None":
                iq_file.iq = copy.deepcopy(self.iq_file.iq)
                iq_file.t_iq_sec = copy.deepcopy(self.iq_file.t_iq_sec)
            else:
                raise Exception('Unexpected self.status["state"]')

            iq_file.write_file()


    def __btn_reset_callback__(self):
        """
        Callback function for Reset button
        :return: void
        """
        self.update_plots()


    #
    # ------------------------------------ End Combobox and Button Callbacks ------------------------------------
    #


    def __update_button_states__(self):
        if self.cbFilterType.currentText() == "None":
            self.btnSelect.setEnabled(False)
        else:
            self.btnSelect.setEnabled(True)

        if self.status["state"] == "None":
            self.btnUpdate.setEnabled(False)
            self.btnExport.setEnabled(False)
            self.btnReset.setEnabled(False)
        else:
            self.btnUpdate.setEnabled(True)
            self.btnExport.setEnabled(True)
            self.btnReset.setEnabled(True)


    def update_selected_frequencies_hz(self):
        self.status["selected_frequencies_hz"] = np.asarray(
            [
                self.psd_layout_widget.frequency_span_selector.boundingRect().left(),
                self.psd_layout_widget.frequency_span_selector.boundingRect().right(),
            ]
        ) * 10 ** (self.settings["frequency_units"].value * 3)


    def remove_plot_widget_selectors(self):
        for layout_widget in self.graphics_layout_widgets:
            layout_widget.remove_selectors()


    def __redraw_figures__(self):
        """
        Re-runs tight_layout() and redraws figures in a way that vertically aligns the axes

        :return:
        """
        max_axis_width = -1
        for plot_widget in [layout_widget.plot_item for layout_widget in self.graphics_layout_widgets]:
            if plot_widget is not None and plot_widget.getAxis("left").width() > max_axis_width:
                max_axis_width = plot_widget.getAxis("left").width()
        max_axis_width = np.max([max_axis_width, 70])
        for plot_widget in [layout_widget.plot_item for layout_widget in self.graphics_layout_widgets]:
            if plot_widget is not None:
                plot_widget.getAxis("left").setWidth(max_axis_width)

        if self.settings["waterfall"]["show_histogram"]:
            min_plot_width = self.width() - 200
        else:
            min_plot_width = self.width() - 100
        for plot_widget in [layout_widget.plot_item for layout_widget in self.graphics_layout_widgets]:
            if plot_widget is not None:
                plot_widget.setMaximumWidth(min_plot_width)
                plot_widget.setMinimumWidth(min_plot_width)
                plot_widget.setPreferredWidth(min_plot_width)


    def import_file(self, filename=None):
        """
        Helper function for selecting, importing, and displaying a new file
        :param filename:
        :return:
        """
        fsd = FileSelectionDialog(selection_mode="import", iq_file_name=filename,
                                  variable_mapping=self.settings["variable_mapping"])
        fsd.exec_()

        if fsd.Accepted:
            self.iq_file = fsd.results.iq_file
            self.selection_parameters = fsd.results.selection_parameters
            self.iq_file.read_file(start_sample=self.selection_parameters['start_sample'],
                                   end_sample=self.selection_parameters['end_sample'])
            if self.selection_parameters['flip_spectrum']:
                self.iq_file.iq = np.real(self.iq_file.iq) - 1j * np.imag(self.iq_file.iq)

            self.iq_file.center_frequency_hz = self.iq_file.center_frequency_hz + \
                                               self.selection_parameters['frequency_offset_hz']

            self.update_plots()

            while fsd.results.iq_file.filename in self.settings["recent_files"]["recent_file_list"]:
                self.settings["recent_files"]["recent_file_list"].remove(fsd.results.iq_file.filename)
            self.settings["recent_files"]["recent_file_list"].insert(0, fsd.results.iq_file.filename)
            while len(self.settings["recent_files"]["recent_file_list"]) > self.settings["recent_files"]["max_num"]:
                self.settings["recent_files"]["recent_file_list"].remove(
                    self.settings["recent_files"]["recent_file_list"][-1]
                )
            self.update_recent_file_actions()
            self.setWindowTitle('IQ File Processor: ' + self.iq_file.filename)
        self.setWindowState(self.windowState() & ~QtCore.Qt.WindowMinimized | QtCore.Qt.WindowActive)
        self.activateWindow()


    def update_plots(self, iq_samples=None, t_sec=None, fs_hz=None, center_frequency_hz=None):
        """
        Updates the 3 plots in the GUI window
        :param iq_samples: IQ to plot
        :param t_sec: Vector of time, units of sec, same length as iq
        :param fs_hz: Sample rate
        :param center_frequency_hz: Center frequency in Hz
        :return:

        Updates:
            1. Time series window
            2. Frequency window
            3. Waterfall window
        """
        if iq_samples is None:
            iq_samples = self.iq_file.iq
        if t_sec is None:
            t_sec = self.iq_file.t_iq_sec
        if fs_hz is None:
            fs_hz = self.iq_file.fs_hz
        if center_frequency_hz is None:
            center_frequency_hz = self.iq_file.center_frequency_hz

        # Remove existing plots if they are present
        for layout_widget in self.graphics_layout_widgets:
            layout_widget.clear()
            layout_widget.parent().layout().removeWidget(layout_widget)
            layout_widget.__del__()

        # Time Series
        if self.settings["time_series"]["show_time_series_plot"]:
            self.time_series_layout_widget = TimeSeriesPlot(iq_samples, t_sec)
            self.time_series_plot_span_selector = self.time_series_layout_widget.time_span_selector
            self.time_series_plot_span_selector.sigRegionChanged.connect(self.__span_region_changed_callback__)
            self.time_series_plot_span_selector.sigRegionChangeFinished.connect(
                self.__span_region_change_finished_callback__)
            self.frame_time_series.layout().addWidget(self.time_series_layout_widget)
            self.graphics_layout_widgets.append(self.time_series_layout_widget)
            self.frame_time_series.show()
        else:
            self.time_series_layout_widget = None
            self.frame_time_series.hide()

        # PSD Spectrum
        if self.settings["psd"]["show_psd_plot"]:
            self.psd_layout_widget = PowerSpectralDensityPlot(iq_samples, fs_hz, center_frequency_hz, self.settings,
                                                              self.signal_statusbar_text)
            self.frame_psd.layout().addWidget(self.psd_layout_widget)
            self.graphics_layout_widgets.append(self.psd_layout_widget)
            self.psd_plot_span_selector = self.psd_layout_widget.frequency_span_selector
            self.psd_plot_span_selector.sigRegionChanged.connect(self.__span_region_changed_callback__)
            self.psd_plot_span_selector.sigRegionChangeFinished.connect(self.__span_region_change_finished_callback__)
            self.frame_psd.show()
        else:
            self.psd_layout_widget = None
            self.frame_psd.hide()

        # Waterfall
        if self.settings["waterfall"]["show_waterfall_plot"]:
            self.waterfall_layout_widget = WaterfallPlot(iq_samples, fs_hz, center_frequency_hz, t_sec[0],
                                                         self.settings)
            self.frame_waterfall.layout().addWidget(self.waterfall_layout_widget)
            self.waterfall_time_span_selector = self.waterfall_layout_widget.time_span_selector
            self.waterfall_time_span_selector.sigRegionChanged.connect(self.__span_region_changed_callback__)
            self.waterfall_frequency_span_selector = self.waterfall_layout_widget.frequency_span_selector
            self.waterfall_frequency_span_selector.sigRegionChanged.connect(self.__span_region_changed_callback__)
            self.waterfall_roi_selector = self.waterfall_layout_widget.time_frequency_roi_selector
            self.graphics_layout_widgets.append(self.waterfall_layout_widget)
            self.frame_waterfall.show()
        else:
            self.waterfall_layout_widget = None
            self.frame_waterfall.hide()

        self.__redraw_figures__()


    def __span_region_changed_callback__(self, roi=None):
        """
        Callback function for when one of the Span Selector objects changes (usually via user interactions)
        :param roi: Region of Interest object
        :return: void
        """
        if self.status["state"] == "Selecting Time Span":
            if roi.parentItem().parentItem().parentItem() == self.time_series_layout_widget.plot_item:
                self.waterfall_layout_widget.time_span_selector.setRegion(
                    self.time_series_layout_widget.time_span_selector.getRegion()
                )
            elif roi.parentItem().parentItem().parentItem() == self.waterfall_layout_widget.plot_item:
                self.time_series_layout_widget.time_span_selector.setRegion(
                    self.waterfall_layout_widget.time_span_selector.getRegion()
                )
            else:
                raise Exception("Parent item not matched to plot widget")
        elif self.status["state"] == "Selecting Frequency Span":
            if self.psd_layout_widget is not None and \
                    roi.parentItem().parentItem().parentItem() == self.psd_layout_widget.plot_item and \
                    self.waterfall_layout_widget is not None:
                self.waterfall_layout_widget.frequency_span_selector.setRegion(
                    self.psd_layout_widget.frequency_span_selector.getRegion()
                )
            elif self.waterfall_layout_widget is not None and \
                    roi.parentItem().parentItem().parentItem() == self.waterfall_layout_widget.plot_item and \
                    self.psd_layout_widget is not None:
                self.psd_layout_widget.frequency_span_selector.setRegion(
                    self.waterfall_layout_widget.frequency_span_selector.getRegion()
                )
            else:
                if self.settings["psd"]["show_psd_plot"] is True and \
                        self.settings["waterfall"]["show_waterfall_plot"] is True:
                    raise Exception("Parent item not matched to plot widget")
        elif self.status["state"] == "Selecting Time+Frequency Box":
            if isinstance(roi, pyqtgraph.graphicsItems.LinearRegionItem.LinearRegionItem):
                if self.time_series_layout_widget is not None and \
                        roi.parentItem().parentItem().parentItem() == self.time_series_layout_widget.plot_item:
                    self.waterfall_roi_selector.setPos(
                        (self.waterfall_roi_selector.pos().x(), self.time_series_plot_span_selector.getRegion()[0]),
                        update=False,
                        finish=False,
                    )
                    self.waterfall_roi_selector.setSize(
                        (
                            self.waterfall_roi_selector.size().x(),
                            np.diff(self.time_series_plot_span_selector.getRegion())[0],
                        ),
                        update=False,
                        finish=False,
                    )
                elif self.psd_layout_widget is not None and \
                        roi.parentItem().parentItem().parentItem() == self.psd_layout_widget.plot_item:
                    self.waterfall_roi_selector.setPos(
                        (self.psd_plot_span_selector.getRegion()[0], self.waterfall_roi_selector.pos().y()),
                        update=False,
                        finish=False,
                    )
                    self.waterfall_roi_selector.setSize(
                        (np.diff(self.psd_plot_span_selector.getRegion())[0], self.waterfall_roi_selector.size().y()),
                        update=False,
                        finish=False,
                    )
                else:
                    raise Exception("Parent item not matched to plot widget")
            else:
                raise Exception("Unrecognized roi type: " + str(type(roi)))
        else:
            # raise Exception("Unexpected Status State: " + self.status["state"])
            return


    def __span_region_change_finished_callback__(self, roi=None):
        """
        Region change callback
        :param roi:
        :return:
        """
        self.status["selected_frequencies_hz"] = np.asarray(
                [
                    roi.shape().boundingRect().left(),
                    roi.shape().boundingRect().right(),
                ]
            ) * 10 ** (self.settings["frequency_units"].value * 3)


    def __waterfall_region_change_finished_callback__(self, roi=None):
        self.status["selected_frequencies_hz"] = np.asarray(
                [
                    roi.shape().boundingRect().left(),
                    roi.shape().boundingRect().right(),
                ]
            ) * 10 ** (self.settings["frequency_units"].value * 3)


    def __get_selection_parameters__(self):
        """
        Helper function to get selection parameters based on selection state and ROI controls
        :return: tuple (bandwidth_hz, center_frequency_hz, end_time_sec, start_time_sec)
        """
        if self.status["last_selection"]["state"] == "Selecting Time+Frequency Box":
            selected_rectangle = self.waterfall_roi_selector.parentBounds()
            bandwidth_hz = np.abs(selected_rectangle.left() - selected_rectangle.right()) * 10 ** (
                self.settings["frequency_units"].value * 3
            )
            center_frequency_hz = np.mean([selected_rectangle.left(), selected_rectangle.right()]) * 10 ** (
                self.settings["frequency_units"].value * 3
            )
            start_time_sec = np.min([selected_rectangle.bottom(), selected_rectangle.top()])
            end_time_sec = np.max([selected_rectangle.bottom(), selected_rectangle.top()])
        elif self.status["last_selection"]["state"] == "Selecting Frequency Span":
            frequencies_hz = self.status["selected_frequencies_hz"]
            bandwidth_hz = np.abs(frequencies_hz[0] - frequencies_hz[1])
            center_frequency_hz = np.mean(frequencies_hz)
            start_time_sec = self.iq_file.t_iq_sec[0]
            end_time_sec = self.iq_file.t_iq_sec[-1]
        elif self.status["last_selection"]["state"] == "Selecting Time Span":
            center_frequency_hz = self.iq_file.center_frequency_hz
            bandwidth_hz = self.iq_file.fs_hz
            times_sec = np.asarray(
                [
                    self.waterfall_time_span_selector.boundingRect().bottom(),
                    self.waterfall_time_span_selector.boundingRect().top(),
                ]
            )
            start_time_sec = np.min(times_sec)
            end_time_sec = np.max(times_sec)
        elif self.status["last_selection"]["state"] == "None":
            center_frequency_hz = self.iq_file.center_frequency_hz
            bandwidth_hz = self.iq_file.fs_hz
            start_time_sec = self.iq_file.t_iq_sec[0]
            end_time_sec = self.iq_file.t_iq_sec[-1]
            self.waterfall_roi_selector.boundingRect()
        else:
            # raise Exception('Unexpected self.status["state"]')
            return
        start_time_dtg = self.iq_file.timecode_datetime + np.timedelta64(int(np.round(1e9 * (start_time_sec - self.iq_file.t_start_sec))), 'ns')
        return bandwidth_hz, center_frequency_hz, end_time_sec, start_time_sec, start_time_dtg


def main():
    """
    Main method
    """
    app = QtWidgets.QApplication(sys.argv)
    app.setOrganizationName("IQFileProcessor")
    app.setOrganizationDomain("https://gitlab.com/ben-mathews/iq_file_processor")
    app.setApplicationName("IQFileProcessor")

    # Ripped off from https://gist.github.com/gph03n1x/7281135
    app.setStyle("Fusion")
    palette = QtGui.QPalette()
    black_level = 35
    palette.setColor(QtGui.QPalette.Window, QtGui.QColor(black_level, black_level, black_level))
    palette.setColor(QtGui.QPalette.WindowText, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Base, QtGui.QColor(15, 15, 15))
    palette.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(black_level, black_level, black_level))
    palette.setColor(QtGui.QPalette.ToolTipBase, QtGui.QColor(black_level, black_level, black_level))
    palette.setColor(QtGui.QPalette.ToolTipText, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Text, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Button, QtGui.QColor(black_level, black_level, black_level))
    palette.setColor(QtGui.QPalette.ButtonText, QtCore.Qt.white)
    palette.setColor(QPalette.Disabled, QtGui.QPalette.ButtonText, QtCore.Qt.darkGray)
    palette.setColor(QPalette.Disabled, QtGui.QPalette.Text, QtCore.Qt.darkGray)
    palette.setColor(QtGui.QPalette.BrightText, QtCore.Qt.red)
    palette.setColor(QtGui.QPalette.Highlight, QtGui.QColor(11, 22, 229).lighter())
    palette.setColor(QtGui.QPalette.HighlightedText, QtCore.Qt.black)
    app.setPalette(palette)

    window = IQFileProcessorMainWindow()
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
